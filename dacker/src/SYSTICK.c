#include "SYSTICK.h"
#include "stm32f4xx.h"

//#define TIMEBASE_TIMER TIM4

#ifndef TIMEBASE_TIMER

static volatile uint32_t uwTick;

void SysTick_Init(uint32_t HCLKFreq, uint32_t Ticks)
{
	SysTick_Config((HCLKFreq / Ticks) - 1);
	SysTick->LOAD = (HCLKFreq / Ticks) - 1;
	SysTick->VAL = 0;
	SysTick->CTRL = SysTick_CTRL_CLKSOURCE_Msk |
					SysTick_CTRL_TICKINT_Msk;

	NVIC_SetPriority(SysTick_IRQn, 0);
	NVIC_EnableIRQ(SysTick_IRQn);

	SysTick->CTRL |= SysTick_CTRL_ENABLE_Msk;
}

void SysTick_Handler(void) { uwTick++; }

uint32_t HAL_GetTick(void) { return uwTick; }


#else // #ifndef TIMEBASE_TIMER

void SysTick_Init(uint32_t HCLKFreq, uint32_t Ticks)
{
	
}

uint32_t HAL_GetTick(void)
{
	return 0;
}

#endif // #ifndef TIMEBASE_TIMER

void HAL_Delay(uint32_t delay)
{
	uint32_t tickstart = HAL_GetTick();

	if (delay < 0xFFFFFFFF)
		delay++;

	while ((HAL_GetTick() - tickstart) < delay);
}
