#ifndef PLL_H
#define PLL_H

#include <stdint.h>

void PLL_Init(void);
uint32_t HAL_RCC_GetHCLKFreq(void);

void PLL_SetOverclock48(void);
void PLL_ResetOverclock48(void);

#endif
