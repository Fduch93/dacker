#ifndef NUM2STR_H
#define NUM2STR_H

#include <stdint.h>

void num2hex(char *out, uint32_t num);
void num2dec(char *out, uint32_t num);
void num2str(char *out, uint32_t num, uint8_t base);

#endif
