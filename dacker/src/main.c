#include "PLL.h"
#include "RTC.h"
#include "GPIO.h"
#include "SYSTICK.h"
#include "LED.h"

#include "DISP.h"
#include "UI_ScopeFrame.h"

#include "stm32f4xx.h"
void enableFpu(void)
{
	SCB->CPACR = (3UL << 10*2) | (3UL << 11*2);
}

#include "wave.h"
#include "autodac.h"

extern Wave_T wave1;

int main(void)
{
	PLL_Init();
	GPIO_Init();
	SysTick_Init(HAL_RCC_GetHCLKFreq(), 1000);

	DISP_Init(UI_GetScopeFrame());

	InitDac(wave1.samples);
	WAVE_Init();
	
	while (1)
	{
		DISP_Handler();

		if (HAL_GetTick() % 500 < 250)
			LED_GreenOn();
		else
			LED_GreenOff();
	}
}
