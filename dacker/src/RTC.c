#include "RTC.h"
#include "stm32f4xx.h"

void RTC_Init(void)
{
	PWR->CR |= PWR_CR_DBP;
	RCC->BDCR |= RCC_BDCR_LSEON;
	while (!(RCC->BDCR & RCC_BDCR_LSERDY));
	RCC->BDCR &= ~RCC_BDCR_RTCSEL_Msk;
	RCC->BDCR |= 1 << RCC_BDCR_RTCSEL_Pos;
	RCC->BDCR |= RCC_BDCR_RTCEN;
}

uint8_t RTC_GetHours(void)
{
	uint8_t hourTens  = (RTC->TR & RTC_TR_HT_Msk) >> RTC_TR_HT_Pos;
	uint8_t hourUnits = (RTC->TR & RTC_TR_HU_Msk) >> RTC_TR_HU_Pos;
	return hourTens * 10 + hourUnits;
}

uint8_t RTC_GetMinutes(void)
{
	uint8_t minuteTens  = (RTC->TR & RTC_TR_MNT_Msk) >> RTC_TR_MNT_Pos;
	uint8_t minuteUnits = (RTC->TR & RTC_TR_MNU_Msk) >> RTC_TR_MNU_Pos;
	return minuteTens * 10 + minuteUnits;
}

uint8_t RTC_GetSeconds(void)
{
	uint8_t secondTens  = (RTC->TR & RTC_TR_ST_Msk) >> RTC_TR_ST_Pos;
	uint8_t secondUnits = (RTC->TR & RTC_TR_SU_Msk) >> RTC_TR_SU_Pos;
	return secondTens * 10 + secondUnits;
}

uint8_t RTC_GetDay(void)
{
	uint8_t dayTens  = (RTC->DR & RTC_DR_DT_Msk) >> RTC_DR_DT_Pos;
	uint8_t dayUnits = (RTC->DR & RTC_DR_DU_Msk) >> RTC_DR_DU_Pos;
	return dayTens * 10 + dayUnits;
}

uint8_t RTC_GetMonth(void)
{
	uint8_t monthTens  = (RTC->DR & RTC_DR_MT_Msk) >> RTC_DR_MT_Pos;
	uint8_t monthUnits = (RTC->DR & RTC_DR_MU_Msk) >> RTC_DR_MU_Pos;
	return monthTens * 10 + monthUnits;
}

uint8_t RTC_GetYear(void)
{
	uint8_t yearTens  = (RTC->DR & RTC_DR_MT_Msk) >> RTC_DR_MT_Pos;
	uint8_t yearUnits = (RTC->DR & RTC_DR_MU_Msk) >> RTC_DR_MU_Pos;
	return yearTens * 10 + yearUnits;
}
