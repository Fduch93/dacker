#include "autodac.h"

#include "stm32f4xx.h"
#include "pll.h"
#include "wave.h"

unsigned PLL_GetTimerClock(void)
{
  return 84000000;
}

static uint32_t strobeOpenDrain = (1U << 5) << 16;
static uint32_t strobeHiZ = 1U << 5;

void InitDac(uint16_t *samples)
{
	RCC->AHB1ENR |= RCC_AHB1ENR_DMA1EN
				 |  RCC_AHB1ENR_DMA2EN;

	RCC->APB1ENR |= RCC_APB1ENR_DACEN
				 |	RCC_APB1ENR_TIM2EN;
	
	RCC->APB2ENR |= RCC_APB2ENR_TIM8EN;

	DAC1->CR = DAC_CR_EN1|DAC_CR_EN2;

	DMA1_Stream1->M0AR = (uint32_t)samples;
	DMA1_Stream1->PAR = (uint32_t)&DAC1->DHR12R2;
	DMA1_Stream1->NDTR = WAVE_SZ;
	DMA1_Stream1->CR = 1 << DMA_SxCR_DIR_Pos // memory to peripherial
					 | 1 << DMA_SxCR_MSIZE_Pos // half-word memory
					 | 1 << DMA_SxCR_PSIZE_Pos // half-word peripherial
					 | DMA_SxCR_MINC // memory increment
					 | DMA_SxCR_CIRC; // circular mode

	DMA1_Stream1->CR |= 3 << DMA_SxCR_CHSEL_Pos; // TIM2 update
	
	DMA1_Stream1->CR |= DMA_SxCR_EN;

	TIM2->PSC = 0;
	TIM2->ARR = 820-1;
	
	TIM2->EGR	= TIM_EGR_UG;
	TIM2->DIER	= TIM_DIER_UDE;
	TIM2->CR1	|= TIM_CR1_ARPE;
	
	
	TIM2->CR2 = 2 << TIM_CR2_MMS_Pos; // update as TRGO
	
	DMA2_Stream2->M0AR = (uint32_t)&strobeOpenDrain;
	DMA2_Stream2->PAR = (uint32_t)&GPIOE->BSRR;
	DMA2_Stream2->NDTR = 1;
	DMA2_Stream2->CR = 1 << DMA_SxCR_DIR_Pos // memory to peripherial
					 | 2 << DMA_SxCR_MSIZE_Pos // word memory
					 | 2 << DMA_SxCR_PSIZE_Pos // word peripherial
					 | DMA_SxCR_CIRC; // circular mode
					 
	DMA2_Stream3->M0AR = (uint32_t)&strobeHiZ;
	DMA2_Stream3->PAR = (uint32_t)&GPIOE->BSRR;
	DMA2_Stream3->NDTR = 1;
	DMA2_Stream3->CR = 1 << DMA_SxCR_DIR_Pos // memory to peripherial
					 | 2 << DMA_SxCR_MSIZE_Pos // word memory
					 | 2 << DMA_SxCR_PSIZE_Pos // word peripherial
					 | DMA_SxCR_CIRC; // circular mode
					 
	DMA2_Stream2->CR |= 7 << DMA_SxCR_CHSEL_Pos; // TIM8 channel 1
	DMA2_Stream3->CR |= 7 << DMA_SxCR_CHSEL_Pos; // TIM8 channel 2
	
	DMA2_Stream2->CR |= DMA_SxCR_EN;
	DMA2_Stream3->CR |= DMA_SxCR_EN;
	
	TIM8->PSC = 0;
	TIM8->ARR = WAVE_SZ-1;
	TIM8->CCR1 = TIM8->ARR/4;
	TIM8->CCR2 = TIM8->ARR/4 + TIM8->ARR/16;
	   
	TIM8->SMCR = 7 << TIM_SMCR_SMS_Pos | 1 << TIM_SMCR_TS_Pos; // count tim2 trgo
	   
	TIM8->CCMR1	= TIM_CCMR1_OC1PE | TIM_CCMR1_OC2PE
				| TIM_CCMR1_OC1M  | TIM_CCMR1_OC2M;
	TIM8->CCER = TIM_CCER_CC1E | TIM_CCER_CC2E;
	TIM8->BDTR = TIM_BDTR_MOE;
	   
	TIM8->EGR	= TIM_EGR_UG;
	TIM8->DIER	= TIM_DIER_CC1DE | TIM_DIER_CC2DE;
	
	DAC1->DHR12R2 = 2048;
	GPIOE->BSRR = strobeHiZ;
}

void StartDac(void)
{
	TIM2->EGR = TIM_EGR_UG;
	
	DMA1_Stream1->CR |= DMA_SxCR_EN;
	DMA2_Stream2->CR |= DMA_SxCR_EN;
	DMA2_Stream3->CR |= DMA_SxCR_EN;

	TIM8->CR1 |= TIM_CR1_CEN;
	TIM2->CR1 |= TIM_CR1_CEN;
}

void StopDac(void)
{
	DMA1_Stream1->CR &= ~DMA_SxCR_EN;
	DMA2_Stream2->CR &= ~DMA_SxCR_EN;
	DMA2_Stream3->CR &= ~DMA_SxCR_EN;
	
	TIM2->CR1 &= ~TIM_CR1_CEN;
	TIM8->CR1 &= ~TIM_CR1_CEN;
	TIM2->CNT = 0;
	TIM8->CNT = 0;
	
	DAC1->DHR12R2 = 2048;
	GPIOE->BSRR = strobeHiZ;
}

void DAC_Config(uint32_t freq, uint32_t strobeStart, uint32_t strobeWidth)
{
	TIM2->ARR = 84000000U/WAVE_SZ/freq-1;
	TIM8->CCR1 = strobeStart;
	TIM8->CCR2 = strobeStart + strobeWidth;
}
