#ifndef STATE_MACHINE_H
#define STATE_MACHINE_H

#include <stdint.h>

typedef struct
{
	uint32_t state;
	uint32_t (*handler)(void);
} SM_State_T;

typedef struct
{
	uint32_t	state;
	uint32_t	state_cnt;
	SM_State_T *states;
} SM_State_Machine_T;

void SM_Init(
	SM_State_Machine_T *sm,
	uint32_t	state_cnt,
	SM_State_T *states,
	uint32_t init_state);

void SM_Process(SM_State_Machine_T *sm);
void SM_SetState(SM_State_Machine_T *sm, uint32_t state);

#endif
