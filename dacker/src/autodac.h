#include <stdint.h>

typedef struct
{
	uint16_t arr;
	uint16_t sampleCount;
} DAC_Configuration_T;

uint32_t DAC_GetSamplingFreq(void);

void InitDac(uint16_t *wave1);
void StartDac(void);
void StopDac(void);

void DAC_Config(uint32_t freq, uint32_t strobeStart, uint32_t strobeWidth);

