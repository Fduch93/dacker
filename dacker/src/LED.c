#include "LED.h"
#include "stm32f4xx.h"

void LED_GreenOn(void)
{
	GPIOA->BSRR = (1<<1) << 16;
}

void LED_GreenOff(void)
{
	GPIOA->BSRR = (1<<1) <<  0;
}
