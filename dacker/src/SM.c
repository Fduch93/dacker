#include "SM.h"

void SM_Init(
	SM_State_Machine_T *sm,
	uint32_t	state_cnt,
	SM_State_T *states,
	uint32_t init_state)
{
	sm->states = states;
	sm->state_cnt = state_cnt;
	sm->state = init_state;
}

void SM_Process(SM_State_Machine_T *sm)
{
	for (uint32_t i = 0; i < sm->state_cnt; i++)
	{
		if (sm->state == sm->states[i].state)
		{
			if (sm->states[i].handler)
				sm->state = sm->states[i].handler();
			return;
		}
	}
}
