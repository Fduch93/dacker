#include "wave.h"
#include "autodac.h"

#include <math.h>
#define PI 3.141592653589793f

Wave_T wave1;

static uint32_t freq = 2400;
static uint32_t ampl = 384;
static uint32_t dist = 0;
static uint32_t strobeStart = WAVE_SZ/4;
static uint32_t strobeWidth = WAVE_SZ/16;
static WAVE_Mode_T mode = DacMode_Sqr;

void WAVE_Init()
{
	DAC_Config(freq,strobeStart,strobeWidth);
	BuildWave();
}

void BuildWave(void)
{	
	int32_t A = ampl-1;
	switch (mode)
	{
		default:
		case DacMode_Sin:
		{
			for (uint32_t s = 0; s < WAVE_SZ; s++)
			{
				int smpl = sinf(2*PI*s/WAVE_SZ) * (1+dist/50.f) * A ;
				if (smpl > +A) smpl = +A;
				if (smpl < -A) smpl = -A;
				wave1.samples[s] = smpl+2048;
			}
			break;
		}
		case DacMode_Sqr:
		{
			uint32_t rampWidth = WAVE_SZ/4*dist/100;
			
			for (uint32_t s = 0; s < rampWidth; s++)
				wave1.samples[s] = 2048 + s*A/rampWidth;
			for (uint32_t s = rampWidth; s < WAVE_SZ/2-rampWidth; s++)
				wave1.samples[s] = 2048 + A;
			for (uint32_t s = WAVE_SZ/2-rampWidth; s < WAVE_SZ/2; s++)
				wave1.samples[s] = 2048 + (WAVE_SZ/2 - s)*A/rampWidth;
			
			for (uint32_t s = WAVE_SZ/2; s < WAVE_SZ/2+rampWidth; s++)
				wave1.samples[s] = 2048 - (s-WAVE_SZ/2)*A/rampWidth;
			for (uint32_t s = WAVE_SZ/2+rampWidth; s < WAVE_SZ-rampWidth; s++)
				wave1.samples[s] = 2048 - A;
			for (uint32_t s = WAVE_SZ-rampWidth; s < WAVE_SZ; s++)
				wave1.samples[s] = 2048 - (WAVE_SZ - s)*A/rampWidth;
			break;
		}
	}
}

char running = 0;

WAVE_Mode_T	WAVE_GetMode(void)			{ return mode; }
void		WAVE_SetMode(WAVE_Mode_T m)
{ 
	mode = m;
	if (running) StopDac();
	BuildWave();
	if (running) StartDac(); 
}

uint32_t	WAVE_GetFreq(void)			{ return freq; }
void		WAVE_SetFreq(uint32_t f)	
{
	freq = f; 
	if (running) StopDac();
	DAC_Config(freq,strobeStart,strobeWidth);
	if (running) StartDac();  
}

uint32_t	WAVE_GetAmpl(void)			{ return ampl; }
void 		WAVE_SetAmpl(uint32_t a)	
{
	ampl = a; 
	if (running) StopDac();
	BuildWave();
	if (running) StartDac();
}

uint32_t	WAVE_GetDist(void)			{ return dist; }
void 		WAVE_SetDist(uint32_t d)	
{
	dist = d;
	if (running) StopDac();
	BuildWave();
	if (running) StartDac();
}

uint32_t	WAVE_GetStrobeStart(void)			{ return strobeStart; }
void 		WAVE_SetStrobeStart(uint32_t d)		
{
	strobeStart = d;
	if (running) StopDac();
	DAC_Config(freq,strobeStart,strobeWidth);
	if (running) StartDac();  
}

uint32_t	WAVE_GetStrobeWidth(void)			{ return strobeWidth; }
void 		WAVE_SetStrobeWidth(uint32_t d)		
{
	strobeWidth = d;
	if (running) StopDac();
	DAC_Config(freq,strobeStart,strobeWidth);
	if (running) StartDac();  
}

uint32_t	WAVE_IsRunning(void)	{ return running; }
void 		WAVE_Start(void)		{ running = 1;
									  StartDac(); }
void 		WAVE_Stop(void)			{ running = 0;
									  StopDac(); }
