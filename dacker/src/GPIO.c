#include "GPIO.h"
#include "stm32f4xx.h"

#define DI 0U // Digital input
#define DO 1U // Digital output
#define AF 2U // Alternative function
#define AN 3U // Analog input

#define PP 0U // Push-pull
#define OD 1U // Open-drain

#define NP 0U // No pull
#define PU 1U // Pull up
#define PD 2U // Pull down

#define LS 0U // Low speed
#define MS 1U // Medium speed
#define HS 2U // High speed
#define FS 3U // Full speed

#define GPIO(s, a,b,c,d, e,f,g,h, i,j,k,l, m,n,o,p)		\
		(a <<0*s | e <<4*s | i <<8*s  | m <<12*s |      \
		 b <<1*s | f <<5*s | j <<9*s  | n <<13*s |      \
		 c <<2*s | g <<6*s | k <<10*s | o <<14*s |      \
		 d <<3*s | h <<7*s | l <<11*s | p <<15*s)
#define AFNC(s, a,b,c,d, e,f,g,h) 	\
		(a <<0*s | e <<4*s |		\
		 b <<1*s | f <<5*s |		\
		 c <<2*s | g <<6*s |		\
		 d <<3*s | h <<7*s)

void GPIO_Init(void)
{
	RCC->AHB1ENR |= RCC_AHB1ENR_GPIOAEN
				 |	RCC_AHB1ENR_GPIOBEN
				 |	RCC_AHB1ENR_GPIOCEN
				 |	RCC_AHB1ENR_GPIODEN
				 |	RCC_AHB1ENR_GPIOEEN;
	
/*	Ports A and B registers reset values aren't all null. Ought to be configured with '|='*/

/*	GPIO PORT A /////// size   0  1  2  3    4  5  6  7    8  9 10 11   12 13 14 15 */ {
	GPIOA->MODER   |= GPIO(2, DI,DO,AN,AN,  AN,AN,AN,AN,  DI,DI,AF,AF,  AF,DI,DI,DI);
	GPIOA->OTYPER  |= GPIO(1, PP,PP,PP,PP,  PP,PP,PP,PP,  PP,OD,OD,PP,  PP,PP,PP,PP);
	GPIOA->PUPDR   |= GPIO(2, PD,NP,NP,NP,  NP,NP,NP,NP,  NP,NP,PU,NP,  NP,NP,NP,NP);
	GPIOA->OSPEEDR |= GPIO(2, LS,LS,LS,LS,  LS,LS,LS,LS,  LS,FS,FS,FS,  FS,LS,LS,LS);
	GPIOA->AFR[0]  |= AFNC(4,  0, 0, 0, 0,   0, 0, 0, 0                            );
	GPIOA->AFR[1]  |= AFNC(4,                              0, 0,10,10,  10, 0, 0, 0);
/*							  Button        ADC1_CH4            USB_FS_ID........	*/
/*								 LED           ADC1_CH5     	...USB_FS_DM.....	*/
/*							        ADC1_CH2      ADC1_CH6  	........USB_FS_DM	*/
/*							           ADC1_CH3      ADC1_CH7 USB_PWR				*/ 
/*											DAC1									*/
/*											   DAC2									*/ }

/*	GPIO PORT B /////// size   0  1  2  3    4  5  6  7    8  9 10 11   12 13 14 15 */ {
	GPIOB->MODER   |= GPIO(2, AN,DO,DI,DI,  DI,DI,DI,DI,  DI,DI,DI,DI,  DO,AF,AF,AF);
	GPIOB->OTYPER  |= GPIO(1, PP,PP,PP,PP,  PP,PP,PP,PP,  PP,PP,PP,PP,  PP,PP,PP,PP);
	GPIOB->PUPDR   |= GPIO(2, NP,NP,NP,NP,  NP,NP,NP,NP,  NP,NP,NP,NP,  PU,PU,PU,PU);
	GPIOB->OSPEEDR |= GPIO(2, LS,FS,LS,LS,  LS,LS,LS,LS,  LS,LS,LS,LS,  LS,LS,LS,LS);
	GPIOB->AFR[0]  |= AFNC(4,  0, 0, 0, 0,   0, 0, 0, 0                            );
	GPIOB->AFR[1]  |= AFNC(4,                              0, 0, 0, 0,   5, 5, 5, 5);
/*							  ADC1_CH8									SPI2_CS           	*/
/*							     DISP_RST								   SPI2_CLK       	*/
/*																			  SPI2_MISO   	*/
/*																			     SPI2_MOSI	*/ }

/*	GPIO PORT C /////// size   0  1  2  3    4  5  6  7    8  9 10 11   12 13 14 15 */ {
	GPIOC->MODER    = GPIO(2, AN,AN,AN,AN,  AN,DO,DI,DI,  AF,AF,AF,AF,  AF,DI,DI,DI);
	GPIOC->OTYPER   = GPIO(1, PP,PP,PP,PP,  PP,PP,PP,PP,  PP,PP,PP,PP,  PP,PP,PP,PP);
	GPIOC->PUPDR    = GPIO(2, NP,NP,NP,NP,  NP,NP,NP,NP,  PU,PU,PU,PU,  PU,NP,NP,NP);
	GPIOC->OSPEEDR  = GPIO(2, LS,LS,LS,LS,  LS,LS,LS,LS,  FS,FS,FS,FS,  FS,LS,LS,LS);
	GPIOC->AFR[0]   = AFNC(4,  0, 0, 0, 0,   0, 0, 0, 0                            );
	GPIOC->AFR[1]   = AFNC(4,                             12,12,12,12,  12, 0, 0, 0);
/*							  ADC1_CH10		ADC1_CH14     SDIO_D0					*/
/*								 ADC1_CH11     DISP_DC		 SDIO_D1				*/
/*									ADC1_CH12					SDIO_D2				*/
/*									   ADC1_CH13				   SDIO_D3			*/
/*																		SDIO_CK		*/ }

/*	GPIO PORT D /////// size   0  1  2  3    4  5  6  7    8  9 10 11   12 13 14 15 */ {
	GPIOD->MODER    = GPIO(2, DI,DI,AF,DI,  DI,DI,DI,DI,  AF,DI,DI,DI,  DI,DI,DI,DI);
	GPIOD->OTYPER   = GPIO(1, PP,PP,PP,PP,  PP,PP,PP,PP,  PP,PP,PP,PP,  PP,PP,PP,PP);
	GPIOD->PUPDR    = GPIO(2, NP,NP,PU,NP,  NP,NP,NP,NP,  NP,NP,NP,NP,  NP,NP,NP,NP);
	GPIOD->OSPEEDR  = GPIO(2, LS,LS,FS,LS,  LS,LS,LS,LS,  LS,LS,LS,LS,  LS,LS,LS,LS);
	GPIOD->AFR[0]   = AFNC(4,  0, 0,12, 0,   0, 0, 0, 0                            );
	GPIOD->AFR[1]   = AFNC(4,                              7, 0, 0, 0,   0, 0, 0, 0);
/*								    SDIO_CMD										*/
/*														  EXTDAC_UART3_TX			*/ }

/*	GPIO PORT E /////// size   0  1  2  3    4  5  6  7    8  9 10 11   12 13 14 15 */ {
	GPIOE->MODER    = GPIO(2, DI,DI,DI,DI,  DI,DO,DI,DI,  DI,AF,DI,AF,  DI,DI,DI,DI);
	GPIOE->OTYPER   = GPIO(1, PP,PP,PP,PP,  PP,OD,PP,PP,  PP,PP,PP,PP,  PP,PP,PP,PP);
	GPIOE->PUPDR    = GPIO(2, NP,NP,PD,NP,  NP,NP,NP,NP,  NP,PD,NP,PD,  NP,NP,NP,NP);
	GPIOE->OSPEEDR  = GPIO(2, LS,LS,LS,LS,  LS,LS,LS,LS,  LS,LS,LS,LS,  LS,LS,LS,LS);
	GPIOE->AFR[0]   = AFNC(4,  0, 0, 0, 0,   0, 0, 0, 0                            );
	GPIOE->AFR[1]   = AFNC(4,                              0, 1, 0, 1,   0, 0, 0, 0);
/*									Btn						 TIM1_CH1				*/
/*																   TIM1_CH2			*/ }

}
