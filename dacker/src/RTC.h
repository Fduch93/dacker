#ifndef RTC_H
#define RTC_H

#include <stdint.h>

void RTC_Init(void);

uint8_t RTC_GetHours(void);
uint8_t RTC_GetMinutes(void);
uint8_t RTC_GetSeconds(void);
uint8_t RTC_GetDay(void);
uint8_t RTC_GetMonth(void);
uint8_t RTC_GetYear(void);

#endif
