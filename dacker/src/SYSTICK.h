#ifndef SYSTICK_H
#define SYSTICK_H

#include <stdint.h>

void SysTick_Init(uint32_t HCLKFreq, uint32_t Ticks);
void HAL_Delay(uint32_t delay);
uint32_t HAL_GetTick(void);

#endif
