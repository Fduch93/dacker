#include "PLL.h"
#include "stm32f4xx.h"

void PLL_Init(void)
{
	RCC->APB1ENR |= RCC_APB1ENR_PWREN;

	PWR->CR |= PWR_CR_VOS;			// Set VOS = 1 to allow 168 MHz HCLK

	RCC->PLLCFGR	=	8 << RCC_PLLCFGR_PLLM_Pos	// PLL and I2S PLL input prescaler
													// 8 MHz HSE / 8 = 1 MHz PLL input
													// (1..2 MHz recommended)

					| 336 << RCC_PLLCFGR_PLLN_Pos	// Main PLL multiplication factor
													// 1 MHz * 336 = 336 MHz VCO

					|	0 << RCC_PLLCFGR_PLLP_Pos	// SYSCLK division factor = 2
													// 336 MHz VCO / 2 = 168 MHz SYSCLK

					|	1 << RCC_PLLCFGR_PLLSRC_Pos	// HSE as PLL and I2S PLL source

					| 	7 << RCC_PLLCFGR_PLLQ_Pos	// USB/SDIO CLK division factor
													// 336 MHz VCO / 7 = 48 MHz USBCLK
													// USB requires 48 MHz clock ONLY

					| 	2 << 28;					// Register reset value

	RCC->CFGR	= 0<<4				// SYSCLK division factor = 1
									// HCLK (core, AHB, mem, DMA) = SYSCLK = 168 MHz

				| 5<<10				// APB1CLK division factor = 4
									// 168 MHz HCLK / 4 = 42 MHz APB1CLK (84 MHz for timers)
									// 42 MHz max allowed

				| 4<<13				// APB2CLK division factor = 2
									// 168 MHz HCLK / 2 = 84 MHz APB2CLK
									// 84 MHz max allowed

				| 8<<16				// RTC division factor
									// 8 MHz HSE / 8 = 1 MHz
									// RTC requires 1 MHz clock ONLY

				| 0<<21				// HSI as MCO1 source
				| 0<<23				// PLLI2S as I2SCLK
				| 0<<24				// MCO1 division factor = 1
									// 16 MHz HSI = MCO1
				| 0<<27				// MCO2 division factor = 1
									// 168 MHz SYSCLK = MCO2
				| 0<<30;			// SYSCLK as MCO2 source

	// 16 Core/AHB clock cycles for all prescalers to update
	for (int i = 0; i < 160; i++) __NOP();

	RCC->CR	|=	1<<16;						// HSE on
	while (!(RCC->CR & RCC_CR_HSERDY));		// Wait for HSE ready

	RCC->CR	|=	RCC_CR_PLLON				// Main PLL on
			|	!RCC_CR_PLLI2SON;			// I2S PLL off
	while (!(RCC->CR & RCC_CR_PLLRDY));		// Wait for main PLL ready

	FLASH->ACR	= FLASH_ACR_PRFTEN 			// Prefetch enable
				| FLASH_ACR_ICEN			// Instruction cache enable
				| FLASH_ACR_DCEN			// Data cache enable
				| FLASH_ACR_LATENCY_5WS;	// 5 wait-states (6 CPU cycles)
											// required for 150..168 MHz HCLK
	FLASH->ACR;								// Read access, recommended by reference manual

	RCC->CFGR	|= RCC_CFGR_SW_PLL;			// PLL as SYSCLK
}

uint32_t HAL_RCC_GetHCLKFreq(void)
{
  return 168000000;
}
