#include <stdint.h>

typedef enum uint32_t
{
	DacMode_Sin = 0,
	DacMode_Sqr,
	DacMode_Tri,
	DacMode_Saw
} WAVE_Mode_T;

#define WAVE_SZ 256


typedef struct
{
	uint32_t frequency;
	WAVE_Mode_T mode;
	uint16_t samples[WAVE_SZ];
	uint16_t sampleCount;
} Wave_T;

void BuildWave(void);

WAVE_Mode_T	WAVE_GetMode(void);
void		WAVE_SetMode(WAVE_Mode_T m);

uint32_t	WAVE_GetFreq(void);
void		WAVE_SetFreq(uint32_t f);

uint32_t	WAVE_GetAmpl(void);
void 		WAVE_SetAmpl(uint32_t a);

uint32_t	WAVE_GetDist(void);
void 		WAVE_SetDist(uint32_t d);

uint32_t	WAVE_GetStrobeStart(void);
void 		WAVE_SetStrobeStart(uint32_t d);
											
uint32_t	WAVE_GetStrobeWidth(void);
void 		WAVE_SetStrobeWidth(uint32_t d);

uint32_t	WAVE_IsRunning(void);
void 		WAVE_Start(void);
void 		WAVE_Stop(void);
