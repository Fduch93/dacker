#ifndef FONT_H
#define FONT_H

#include <stdint.h>
#include "DISP_color.h"
#include "DISP_coord.h"

typedef struct
{
	uint8_t (*getStringWidth)(const char *str);
	uint8_t (*getHeight)(void);
	void (*writeString)(
		const char *str,
		uint8_t *buf,
		DISP_Rect_T bufRect,
		DISP_Rect_T textRect,
		DISP_Color_T fgColor,
		DISP_Color_T bgColor);
} Font_T;

#endif
