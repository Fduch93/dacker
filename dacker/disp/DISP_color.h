#ifndef DISP_COLOR_H
#define DISP_COLOR_H

#include <stdint.h>

typedef union
{
	uint32_t argb;
	struct
	{
		uint8_t b;
		uint8_t g;
		uint8_t r;
		uint8_t a;
	};
} DISP_Color_T;

extern const DISP_Color_T COLOR_White;
extern const DISP_Color_T COLOR_Silver;
extern const DISP_Color_T COLOR_Grey;
extern const DISP_Color_T COLOR_DarkGrey;
extern const DISP_Color_T COLOR_Black;

extern const DISP_Color_T COLOR_Red;
extern const DISP_Color_T COLOR_MediumRed;
extern const DISP_Color_T COLOR_Maroon;
extern const DISP_Color_T COLOR_DarkRed;

extern const DISP_Color_T COLOR_Blue;
extern const DISP_Color_T COLOR_MediumBlue;
extern const DISP_Color_T COLOR_Navy;
extern const DISP_Color_T COLOR_DarkBlue;

extern const DISP_Color_T COLOR_Lime;
extern const DISP_Color_T COLOR_MediumGreen;
extern const DISP_Color_T COLOR_Green;
extern const DISP_Color_T COLOR_DarkGreen;

extern const DISP_Color_T COLOR_Yellow;
extern const DISP_Color_T COLOR_Cyan;
extern const DISP_Color_T COLOR_Magenta;

extern const DISP_Color_T COLOR_DarkYellow;

#endif
