#undef USE_HAL_DRIVER
#include "SYSTICK.h"
#include "stm32f4xx.h"
#include "ST7735_hal.h"

#define DELAY for (int i = 0; i < 8; i++) __NOP();

void ST7735_HAL_OffByReset(void)
{
	GPIOB->ODR &= ~(1<<1);
	HAL_Delay(200);
}

void ST7735_HAL_OnByReset(void)
{
	GPIOB->ODR |= (1<<1);
	HAL_Delay(200);
}

void ST7735_HAL_SetCommandMode(void)
{
	DELAY;
	GPIOC->ODR &= ~(1<<5);
	DELAY;
}

void ST7735_HAL_SetDataMode(void)
{
	DELAY;
	GPIOC->ODR |= (1<<5);
	DELAY;
}

void ST7735_HAL_ChipSelect(void)
{
	DELAY;
	GPIOB->ODR &= ~(1<<12);
	DELAY;
}

void ST7735_HAL_ChipDeselect(void)
{
	DELAY;
	GPIOB->ODR |= (1<<12);
	DELAY;
}

void ST7735_HAL_Init(void)
{
	RCC->APB1ENR |= RCC_APB1ENR_SPI2EN;

	SPI2->CR1 = !SPI_CR1_BIDIMODE	// unidirectional MOSI & MISO
			| !SPI_CR1_BIDIOE		// no BiDi
			| !SPI_CR1_CRCEN		// no crc
			| !SPI_CR1_CRCNEXT		// no crc
			| !SPI_CR1_DFF			// 8 bit data frame format
			| !SPI_CR1_RXONLY		// no readonly
			| !SPI_CR1_SSM			// software CS enabled
			| SPI_CR1_SSI			// software CS set high
			| !SPI_CR1_LSBFIRST		// MSB first
			| ((0 << SPI_CR1_BR_0) & SPI_CR1_BR)	// PCLK / (2 ^ (1+0)) = 42 MHz / 2 = 21 MHz
			| SPI_CR1_MSTR			// master configuration
			| !SPI_CR1_CPOL			// clk polarity low
			| !SPI_CR1_CPHA;		// 1st clk transition = first data capture edge

	SPI2->CR2 = !SPI_CR2_TXEIE		// no tx buffer empty interrupt
			| !SPI_CR2_RXNEIE		// no rx buffer not empty interrupt
			| !SPI_CR2_ERRIE		// no error interrupt
			| SPI_CR2_SSOE			// !CS output enable in idle state, no multimaster
			| SPI_CR2_TXDMAEN		// tx buffer dma rq
			| !SPI_CR2_RXDMAEN;		// no rx buffer dma rq

	SPI2->CR1 |= SPI_CR1_SPE;		// enable spi

	RCC->AHB1ENR |= RCC_AHB1ENR_DMA1EN;
	DMA1_Stream4->PAR  = (uint32_t)&SPI2->DR;
	DMA1_Stream4->CR = (0 << DMA_SxCR_CHSEL_Pos)	// SPI2
					 | (1 << DMA_SxCR_DIR_Pos)		// memory to peripherial
					 | (0 << DMA_SxCR_CIRC_Pos)		// no circular mode
					 | (0 << DMA_SxCR_PINC_Pos)		// do not increment peripherial address
					 | (1 << DMA_SxCR_MINC_Pos)		// increment memory address
					 | (0 << DMA_SxCR_PSIZE_Pos)	// peripherial data size word
					 | (0 << DMA_SxCR_MSIZE_Pos)	// don't care in direct mode
					 | (0 << DMA_SxCR_DBM_Pos)		// no double buffer mode
					 | (1 << DMA_SxCR_TCIE_Pos);		// transfer complete interrupt enable
//	DMA1_Stream4->CR = DMA_Channel_0 // ADC1
//					 | DMA_DIR_MemoryToPeripheral
//					 | DMA_MemoryDataSize_Byte // don't care in direct mode
//					 | DMA_MemoryInc_Enable
//					 | DMA_PeripheralDataSize_Byte
//					 | DMA_IT_TC;
	NVIC_EnableIRQ(DMA1_Stream4_IRQn);
}

void DMA1_Stream4_IRQHandler()
{
	DMA1->HIFCR = DMA_HISR_FEIF4
				| DMA_HISR_DMEIF4
				| DMA_HISR_TEIF4
				| DMA_HISR_HTIF4
				| DMA_HISR_TCIF4;
	ST7735_DMA_WriteCompleteHandler();
}

void ST7735_HAL_Abort()
{
	DMA1_Stream4->CR &= ~ DMA_SxCR_EN;
	SPI2->CR1 &= ~SPI_CR1_SPE;
	//TODO: reset display controller
}

void ST7735_HAL_Write(const uint8_t byte)
{
	while (!(SPI2->SR & SPI_SR_TXE));
	*(uint8_t *)(&SPI2->DR) = byte;
}

void ST7735_HAL_WriteByte(const uint8_t byte)
{
	SPI2->DR = byte;
}

void ST7735_HAL_WriteArray(const uint8_t *buf, uint32_t cnt)
{
	while (!(SPI2->SR & SPI_SR_TXE));
	while (SPI2->SR & SPI_SR_BSY);
	DMA1_Stream4->M0AR = (uint32_t)buf;
	DMA1_Stream4->NDTR = cnt;
	DMA1_Stream4->CR |= DMA_SxCR_EN;
	while (DMA1_Stream4->NDTR != 0 || (DMA1_Stream4->CR & DMA_SxCR_EN));
}

void ST7735_HAL_StartWriteArray(const uint8_t *buf, uint32_t cnt)
{
	DMA1_Stream4->M0AR = (uint32_t)buf;
	DMA1_Stream4->NDTR = cnt;
	DMA1_Stream4->CR |= DMA_SxCR_EN;
}

char ST7735_HAL_IsWriteComplete(void)
{
	return !((DMA1_Stream4->CR & DMA_SxCR_EN) || (SPI2->SR & SPI_SR_BSY));
}

void ST7735_HAL_Wait(void)
{
	while (!(SPI2->SR & SPI_SR_TXE));
	while (SPI2->SR & SPI_SR_BSY);
}

char ST7735_HAL_IsReadyToWrite(void)
{
	return (SPI2->SR & SPI_SR_TXE) && !(SPI2->SR & SPI_SR_BSY);
}

uint8_t ST7735_HAL_Read()
{
	while (!(SPI2->SR & SPI_SR_RXNE));
	return SPI2->DR;
}
