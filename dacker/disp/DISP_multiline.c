#include "DISP_multiline.h"
#include "DISP_render.h"

void DISP_DrawMultiline(
	const DISP_Object_T *obj,
	const Font_T *font,
	const DISP_Line_T *lines,
	const uint32_t lineCnt,
	DISP_Color_T fgColor,
	DISP_Color_T bgColor,
	uint32_t startLine,
	uint32_t *pCurrentLine,
	DISP_MultilineDirection_T direction)
{
	uint8_t lineHeight = font->getHeight() + 2;

	DISP_Object_T lineObject = {0};
	lineObject.bounds.left = obj->bounds.left;
	lineObject.bounds.right = obj->bounds.right;

	if (direction == DISP_MULTILINE_REVERSED)
	{
		lineObject.bounds.bottom = obj->bounds.bottom - lineHeight * *pCurrentLine;
		lineObject.bounds.top = lineObject.bounds.bottom - lineHeight;
	}
	else
	{
		lineObject.bounds.top = obj->bounds.top + lineHeight * *pCurrentLine;
		lineObject.bounds.bottom = lineObject.bounds.top + lineHeight;
	}

	char *lineText = "";
	if (startLine + *pCurrentLine < lineCnt)
	{
		const DISP_Line_T *line = &lines[startLine + *pCurrentLine];
		lineText = DISP_MakeString(line->pattern, line->var);
	}

	DISP_DrawText(
		&lineObject,
		font,
		lineText,
		fgColor,
		bgColor,
		TEXT_HLEFT,
		TEXT_VTOP,
		0);

	if (direction == DISP_MULTILINE_REVERSED)
	{
		(*pCurrentLine)++;
		if (obj->bounds.bottom  < obj->bounds.top + lineHeight * (*pCurrentLine+1))
			*pCurrentLine = 0;
	}
	else
	{
		if (lineObject.bounds.bottom + lineHeight > obj->bounds.bottom)
			*pCurrentLine = 0;
		else
			(*pCurrentLine)++;
	}
}
