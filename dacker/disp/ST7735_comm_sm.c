#include "SYSTICK.h"
#include "ST7735_hal.h"
#include "SM.h"

typedef enum
{
	ST7735_STATE_IDLE,
	ST7735_STATE_CMD,
	ST7735_STATE_SEND,
	ST7735_STATE_RECEIVE,
	ST7735_STATE_READY
} ST7735_State_T;

typedef struct
{
	uint8_t command;
	const uint8_t *request;
	uint32_t request_len;
	uint8_t *response;
	uint32_t response_len;
	char read_delay;
} ST7735_Request_T;

static void ST7735_ErrorHandler(uint32_t error_code);
static uint32_t ST7735_CmdHandler(void);
static uint32_t ST7735_SendHandler(void);

#define ST7735_STATE_CNT 5
static ST7735_Request_T current_request = {0};
static SM_State_Machine_T ST7735_sm;
static SM_State_T states[ST7735_STATE_CNT] =
{
	{ ST7735_STATE_IDLE,	0						},
	{ ST7735_STATE_CMD,		ST7735_CmdHandler		},
	{ ST7735_STATE_SEND,	ST7735_SendHandler		},
	{ ST7735_STATE_RECEIVE,	0},//ST7735_ReceiveHandler	},
	{ ST7735_STATE_READY,	0}//ST7735_ReadyHandler		}
};

static char isDataSent = 0;
static uint32_t singleTransferDataCnt = 0;

void ST7735_InitStateMachine()
{
	SM_Init(&ST7735_sm, ST7735_STATE_CNT, states, ST7735_STATE_IDLE);
}

char ST7735_IsReady()
{
	return ST7735_sm.state == ST7735_STATE_IDLE;
}

void ST7735_ProcessStateMachine()
{
	SM_Process(&ST7735_sm);
}

void ST7735_ProcessStateMachineUntilDone()
{
	while (!ST7735_IsReady())
		ST7735_ProcessStateMachine();
}

void ST7735_StartRequest(uint8_t cmd, const uint8_t *request, uint16_t req_len, uint8_t *response, uint16_t resp_len, char read_delay)
{
	if (ST7735_sm.state != ST7735_STATE_IDLE)
	{
		ST7735_ErrorHandler(1488);
		return;
	}
	
	current_request.command = cmd;
	current_request.request = request;
	current_request.request_len = req_len;
	current_request.response = response;
	current_request.response_len = resp_len;
	current_request.read_delay = read_delay;
	
	ST7735_HAL_SetCommandMode();
	ST7735_HAL_ChipSelect();
	
	ST7735_HAL_WriteByte(cmd);
	ST7735_sm.state = ST7735_STATE_CMD;
}

static void ST7735_ErrorHandler(uint32_t error_code)
{
	ST7735_HAL_Abort();
	ST7735_sm.state = ST7735_STATE_IDLE;
	
	while (error_code) error_code = error_code;
}

static uint32_t ST7735_CmdHandler()
{
	if (!ST7735_HAL_IsReadyToWrite())
	{
		return ST7735_STATE_CMD;
	}
	else
	{
		if (current_request.request && current_request.request_len)
		{
			ST7735_HAL_SetDataMode();
			isDataSent = 0;
			
			if ((uint32_t)current_request.request & ARRAY_WRITE_ADDRESS_MASK)
				ST7735_HAL_StartWriteArray(
					current_request.request,
					current_request.request_len);
			else
				singleTransferDataCnt = current_request.request_len;
			
			return ST7735_STATE_SEND;
		}
		else if (current_request.response && current_request.response_len)
		{
			//TODO: implement bidirectional communication
//			ST7735_HAL_SetDataMode();
//			
//			DMA1_Stream4->M0AR = (uint32_t)current_request.request;
//			DMA1_Stream4->NDTR = current_request.request_len;
//			DMA1_Stream4->CR |= DMA_SxCR_EN;
			ST7735_HAL_ChipDeselect();
			return ST7735_STATE_IDLE;
		}
		else
		{
			ST7735_HAL_ChipDeselect();
			return ST7735_STATE_IDLE;
		}
	}
}

static uint32_t ST7735_SendHandler(void)
{
	if (isDataSent)
	{
		ST7735_HAL_ChipDeselect();
		return ST7735_STATE_IDLE; //TODO: implement bidirectional communication
	}
	else
	{
		if (ST7735_HAL_IsReadyToWrite())
		{
			if (singleTransferDataCnt > 0)
			{
				uint32_t requestByteIndex = current_request.request_len - singleTransferDataCnt;
				uint8_t byteToWrite = current_request.request[requestByteIndex]; 
				ST7735_HAL_WriteByte(byteToWrite);
				singleTransferDataCnt--;
			}
			else
			{
				isDataSent = 1;
			}
		}
		
		return ST7735_STATE_SEND;
	}
}

void ST7735_DMA_WriteCompleteHandler()
{
	switch (ST7735_sm.state)
	{
		case ST7735_STATE_CMD: // in case data is sent before state is changed
		case ST7735_STATE_SEND:
			if (current_request.response && current_request.response_len)
			{
				//TODO: implement bidirectional communication
//				DMA1_Stream4->M0AR = (uint32_t)current_request.request;
//				DMA1_Stream4->NDTR = current_request.request_len;
//				DMA1_Stream4->CR |= DMA_SxCR_EN;
//				ST7735_sm.state = ST7735_STATE_IDLE;
			}
			else
			{
				isDataSent = 1;
			}
			break;
		case ST7735_STATE_IDLE:
		case ST7735_STATE_RECEIVE:
		case ST7735_STATE_READY:
		default:
			ST7735_ErrorHandler(8841);
			break;
	}
}
