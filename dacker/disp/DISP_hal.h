#ifndef DISP_HAL_H
#define DISP_HAL_H

void DISP_InitEncoder(void);

char DISP_HAL_nextObject(void);
char DISP_HAL_prevObject(void);
char DISP_HAL_selectObject(void);

#endif
