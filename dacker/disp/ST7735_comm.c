#include "SYSTICK.h"
#include "ST7735_comm_sm.h"
#include "SM.h"

void ST7735_SimpleCmd(uint8_t cmd)
{
	ST7735_StartRequest(cmd, 0,0,0,0,0);
	ST7735_ProcessStateMachineUntilDone();
}

void ST7735_CmdWithArgs(uint8_t cmd, const uint8_t *args, uint16_t args_len)
{
	ST7735_StartRequest(cmd, args, args_len, 0,0,0);
	ST7735_ProcessStateMachineUntilDone();
}

void ST7735_SimpleRequest(uint8_t cmd, uint8_t *response, uint16_t resp_len)
{
	ST7735_StartRequest(cmd, 0, 0, response, resp_len, 1);
	ST7735_ProcessStateMachineUntilDone();
}

void ST7735_SimpleRequestNoDummyRead(uint8_t cmd, uint8_t *response, uint16_t resp_len)
{
	ST7735_StartRequest(cmd, 0, 0, response, resp_len, 0);
	ST7735_ProcessStateMachineUntilDone();
}

void ST7735_StartCmdWithArgs(uint8_t cmd, const uint8_t *args, uint16_t args_len)
{
	ST7735_StartRequest(cmd, args, args_len, 0,0,0);
}
