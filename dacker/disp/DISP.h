#ifndef DISP_H
#define DISP_H

#include "DISP_frame.h"

void DISP_Init(DISP_Frame_T *initFrame);
DISP_Frame_T *DISP_GetFrame(void);
void DISP_SetFrame(DISP_Frame_T *frame);

void DISP_SetIncDecAction(
	void (*incrementAction)(void),
	void (*decrementAction)(void));
void DISP_ResetIncDecAction(void);
char DISP_IsIncDecActionSet(void);

void DISP_Handler(void);
void DISP_Refresh(void);

#endif
