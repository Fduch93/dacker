#include "ST7735_comm_sm.h"
#include "ST7735_comm.h"
#include "ST7735_commands.h"

#include "DISP_object.h"
#include "DISP_color.h"

#define GBUF_SIZE 0xF000
static uint8_t gbuf[GBUF_SIZE] = {0};

uint8_t *GetObjectBuffer(void)
{
	return gbuf;
}

void ST7735_SetRenderRect(uint8_t left, uint8_t top, uint8_t right, uint8_t bottom)
{
	uint8_t cs[4] = {0, left+1, 0, right};
	uint8_t rs[4] = {0, top+2, 0, bottom+1};
	ST7735_CmdWithArgs(ST7735_CASET, cs, 4);
	ST7735_CmdWithArgs(ST7735_RASET, rs, 4);
}

void ST7735_Render(uint8_t left, uint8_t top, uint8_t right, uint8_t bottom, const uint8_t *buf)
{
	ST7735_SetRenderRect(left, top, right, bottom);
	ST7735_CmdWithArgs(ST7735_RAMWR, buf, (uint8_t)(right - left) * (uint8_t)(bottom - top) * 3);
//	ST7735_StartCmdWithArgs(ST7735_RAMWR, buf, (uint8_t)(right - left) * (uint8_t)(bottom - top) * 3);
}

void ST7735_DrawObject(const DISP_Object_T *obj)
{
	if (!ST7735_IsReady())
		return;

	ST7735_Render(
		obj->bounds.left,
		obj->bounds.top,
		obj->bounds.right,
		obj->bounds.bottom,
		gbuf);
}

void ST7735_FillRect(
	uint8_t left,
	uint8_t top,
	uint8_t right,
	uint8_t bottom,
	DISP_Color_T (*pixelBuilder)(uint8_t x, uint8_t y))
{
	uint8_t width = right - left;
	uint8_t height = bottom - top;

	if (width * height * 3 <= GBUF_SIZE)
	{
		for (uint8_t x = 0; x < width; x++)
		for (uint8_t y = 0; y < height; y++)
		{
			DISP_Color_T pixelColor = pixelBuilder(x, y);
			int pixelAddress = (x + y * width) * 3;
			gbuf[pixelAddress + 0] = pixelColor.r;
			gbuf[pixelAddress + 1] = pixelColor.g;
			gbuf[pixelAddress + 2] = pixelColor.b;
		}
		ST7735_Render(left, top, right, bottom, gbuf);
	}
	else
	{
		uint8_t chunkCount = width * height * 3 / GBUF_SIZE;
		chunkCount += (chunkCount * GBUF_SIZE < width * height * 3) ? 1 : 0;
		uint8_t chunkWidth = width / chunkCount;
		for (uint8_t x0 = 0; x0 < width; x0 += chunkWidth)
		{
			uint8_t x1 =
				(x0 + chunkWidth) < right
					? x0 + chunkWidth
					: right;

			for (uint8_t y = 0; y < height; y++)
			for (uint8_t x = 0; x < x1 - x0; x++)
			{
				DISP_Color_T pixelColor = pixelBuilder(x, y);
				int pixelAddress = (x + y * chunkWidth) * 3;
				gbuf[pixelAddress + 0] = pixelColor.r;
				gbuf[pixelAddress + 1] = pixelColor.g;
				gbuf[pixelAddress + 2] = pixelColor.b;
			}
			ST7735_Render(x0, top, x1, bottom, gbuf);
			ST7735_ProcessStateMachineUntilDone();
		}
	}
}
