#ifndef DISP_OBJECT_H
#define DISP_OBJECT_H

#include "DISP_coord.h"

#define OBJECT_NO_ACTION 0
#define ALWAYS_ENABLED 0

typedef struct _DISP_Object_T
{
	DISP_Bounds_T bounds;

	void (*update)(const struct _DISP_Object_T *obj, char isSelected);
	void (*action)(void);
	char (*isEnabled)(void);
} DISP_Object_T;

#endif
