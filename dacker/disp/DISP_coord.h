#ifndef DISP_COORD_H
#define DISP_COORD_H

#include <stdint.h>

typedef struct
{
	uint8_t left;
	uint8_t top;
	uint8_t right;
	uint8_t bottom;
} DISP_Bounds_T;

typedef struct
{
	uint8_t left;
	uint8_t top;
	uint8_t width;
	uint8_t height;
} DISP_Rect_T;

#endif
