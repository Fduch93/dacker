#ifndef ST7335_HAL_H
#define ST7335_HAL_H

#include <stdint.h>

#define ARRAY_WRITE_ADDRESS_MASK (0x28000000U)

void ST7735_HAL_OffByReset(void);
void ST7735_HAL_OnByReset(void);
void ST7735_HAL_SetCommandMode(void);
void ST7735_HAL_SetDataMode(void);
void ST7735_HAL_ChipSelect(void);
void ST7735_HAL_ChipDeselect(void);

void ST7735_HAL_Init(void);

void ST7735_HAL_Abort(void);

void ST7735_HAL_WriteByte(const uint8_t byte);
void ST7735_HAL_StartWriteArray(const uint8_t *buf, uint32_t cnt);
char ST7735_HAL_IsWriteComplete(void);
char ST7735_HAL_IsReadyToWrite(void);
//uint8_t ST7735_HAL_Read(void);

void ST7735_DMA_WriteCompleteHandler(void); // external implementation required

#endif
