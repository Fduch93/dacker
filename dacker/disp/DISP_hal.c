#undef USE_HAL_DRIVER
#include "SYSTICK.h"
#include "stm32f4xx.h"

#define NEXT_CONDITION ((int16_t)TIM1->CNT < -ENCODER_THRESHOLD)
#define PREV_CONDITION ((int16_t)TIM1->CNT >  ENCODER_THRESHOLD)
#define SELECT_CONDITION (GPIOE->IDR & (1<<2))
#define ENCODER_THRESHOLD 3
#define DELAY 100

void DISP_InitEncoder()
{
	RCC->APB2ENR |= RCC_APB2ENR_TIM1EN; // 84 MHz APB2CLK * 2 = 168 MHz APB2CLK_TIM
	TIM1->ARR = 0xFFFF;
	TIM1->SMCR |= 3 * TIM_SMCR_SMS_0;
	TIM1->CCMR1 |= 1 * TIM_CCMR1_CC1S_0
				|  1 * TIM_CCMR1_CC2S_0
				|  3 * TIM_CCMR1_IC1F_0
				|  3 * TIM_CCMR1_IC2F_0;
	TIM1->EGR = TIM_EGR_UG;
	TIM1->CR1 |= TIM_CR1_CEN;
}

char DISP_HAL_nextObject()
{
	static unsigned nextObjectTime = 0;
	
	if (NEXT_CONDITION &&
		HAL_GetTick() - nextObjectTime > DELAY)
	{
		nextObjectTime = HAL_GetTick();
		TIM1->CNT = 0;
		return 1;
	}
	
	return 0;
}

char DISP_HAL_prevObject()
{
	static unsigned prevObjectTime = 0;
	
	if (PREV_CONDITION && 
		HAL_GetTick() - prevObjectTime > DELAY)
	{
		prevObjectTime = HAL_GetTick();
		TIM1->CNT = 0;
		return 1;
	}
	
	return 0;
}

char DISP_HAL_selectObject()
{
	static unsigned selectObjectTime = 0;
	static char objectSelected = 0;
	
	if (SELECT_CONDITION)
	{
		selectObjectTime = HAL_GetTick();
		
		if (!objectSelected)
		{
			objectSelected = 1;
			return 1;
		}
	}
	else
	{
		if (HAL_GetTick() - selectObjectTime > DELAY)
			objectSelected = 0;
	}
	
	return 0;
}
