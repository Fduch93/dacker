#include "DISP_font.h"
#include <stdint.h>
#include <string.h>

static const uint8_t TNR12_data[];

static uint8_t getHeight(void) { return 13; }

static uint8_t *getCharPtr(char c)
{
	c -= ' ';
	uint8_t *cptr = (uint8_t *)TNR12_data;
	for (uint8_t i = 0; i < (uint8_t)c; i++)
	{
		cptr+= (*cptr)*getHeight(); // skip char data
		cptr++; // skip char width
		cptr++; // skip char effective width
	}
	return cptr;
}

static uint8_t getStringWidth(const char *str)
{
	uint8_t width = 0;
	while (*(str+1))
	{
		uint8_t *cptr = getCharPtr(*str);
		cptr++; // skip char data size
		uint8_t charEffWidth = *cptr++;

		width += charEffWidth;
		str++;
	}

	uint8_t *cptr = getCharPtr(*str);
	width += *cptr; // add last char full width

	return width;
}

static void writeString(
	const char* str,
	uint8_t *buf,
	DISP_Rect_T bufRect,
	DISP_Rect_T textRect,
	DISP_Color_T fgColor,
	DISP_Color_T bgColor)
{
	uint8_t x0 = textRect.left;
	uint8_t y0 = textRect.top;
	while (*str && x0 < textRect.left + textRect.width)
	{
		uint8_t *cptr = getCharPtr(*str);

		uint8_t charWidth = *cptr;
		cptr++; // skip char size
		uint8_t charEffWidth = *cptr;
		cptr++; // skip char effective width
		uint8_t charHeight = getHeight();

		for (uint8_t x = 0; (x < charWidth) && (x0 + x < textRect.left + textRect.width); x++)
		for (uint8_t y = 0; (y < charHeight) && (y < textRect.height); y++)
		{
			uint16_t alpha = cptr[x + y * charWidth];
			if (alpha)
			{
				DISP_Color_T color;
				color.r = (uint16_t)((uint16_t)fgColor.r * alpha + (uint16_t)bgColor.r * (255 - alpha)) >> 8;
				color.g = (uint16_t)((uint16_t)fgColor.g * alpha + (uint16_t)bgColor.g * (255 - alpha)) >> 8;
				color.b = (uint16_t)((uint16_t)fgColor.b * alpha + (uint16_t)bgColor.b * (255 - alpha)) >> 8;
				buf[3 * (x + x0 + (y + y0) * bufRect.width) + 0] = color.r;
				buf[3 * (x + x0 + (y + y0) * bufRect.width) + 1] = color.g;
				buf[3 * (x + x0 + (y + y0) * bufRect.width) + 2] = color.b;
			}
			else
			{
				buf[3 * (x + x0 + (y + y0) * bufRect.width) + 0] = bgColor.r;
				buf[3 * (x + x0 + (y + y0) * bufRect.width) + 1] = bgColor.g;
				buf[3 * (x + x0 + (y + y0) * bufRect.width) + 2] = bgColor.b;
			}
		}
		x0 += charEffWidth;
		str++;
	}
}

Font_T TNR12 =
{
	getStringWidth,
	getHeight,
	writeString
};

static const uint8_t TNR12_data[] = {
	3, 3, //
	0,	0,	0,
	0,	0,	0,
	0,	0,	0,
	0,	0,	0,
	0,	0,	0,
	0,	0,	0,
	0,	0,	0,
	0,	0,	0,
	0,	0,	0,
	0,	0,	0,
	0,	0,	0,
	0,	0,	0,
	0,	0,	0,

	2, 2, // !
	0,	0,
	150,187,
	187,187,
	150,150,
	150,150,
	150,102,
	150,102,
	90,	76,
	0,	0,
	187,187,
	0,	0,
	0,	0,
	0,	0,

	5, 5, // "
	0,	0,	0,	0,	0,
	76,	201,100,201,24,
	76,	177,76,	201,24,
	38,	128,38,	166,0,
	0,	0,	0,	0,	0,
	0,	0,	0,	0,	0,
	0,	0,	0,	0,	0,
	0,	0,	0,	0,	0,
	0,	0,	0,	0,	0,
	0,	0,	0,	0,	0,
	0,	0,	0,	0,	0,
	0,	0,	0,	0,	0,
	0,	0,	0,	0,	0,

	8, 8, // #
	0,	0,	0,	0,	0,	0,	0,	0,
	0,	0,	24,	142,0,	90,	76,	0,
	0,	0,	38,	128,0,	128,38,	0,
	0,	0,	76,	90,	0,	142,24,	0,
	24,	211,255,255,255,255,211,24,
	0,	0,	128,38,	38,	128,0,	0,
	24,	211,255,255,255,255,211,24,
	0,	24,	142,0,	90,	76,	0,	0,
	0,	38,	128,0,	128,38,	0,	0,
	0,	76,	90,	0,	116,0,	0,	0,
	0,	0,	0,	0,	0,	0,	0,	0,
	0,	0,	0,	0,	0,	0,	0,	0,
	0,	0,	0,	0,	0,	0,	0,	0,

	6, 6, // $
	0,	0,	90,	76,	0,	0,
	0,	187,255,255,102,0,
	102,187,90,	76,	166,38,
	102,211,114,76,	0,	0,
	24,	211,255,102,0,	0,
	0,	24,	211,255,102,0,
	0,	0,	90,	167,244,76,
	0,	0,	90,	76,	187,102,
	102,102,90,	100,201,76,
	0,	187,255,255,150,0,
	0,	0,	90,	76,	0,	0,
	0,	0,	0,	0,	0,	0,
	0,	0,	0,	0,	0,	0,

	10, 9, // %
	0,	0,	0,	0,	0,	0,	0,	0,	0,	0,
	38,	233,255,102,0,	0,	0,	166,38,	0,
	150,150,102,211,24,	0,	102,102,0,	0,
	150,150,102,211,24,	38,	166,0,	0,	0,
	76,	244,255,102,0,	166,38,	0,	0,	0,
	0,	0,	0,	0,	76,	90,	0,	0,	0,	0,
	0,	0,	0,	24,	167,24,	76,	244,244,76,
	0,	0,	0,	139,76,	0,	187,102,102,187,
	0,	0,	76,	139,0,	0,	187,102,102,187,
	0,	38,	190,24,	0,	0,	76,	244,244,76,
	0,	0,	0,	0,	0,	0,	0,	0,	0,	0,
	0,	0,	0,	0,	0,	0,	0,	0,	0,	0,
	0,	0,	0,	0,	0,	0,	0,	0,	0,	0,

	10, 10, // &
	0,	0,	0,	0,	0,	0,	0,	0,	0,	0,
	0,	0,	38,	233,255,150,0,	0,	0,	0,
	0,	0,	150,150,24,	190,38,	0,	0,	0,
	0,	0,	150,150,76,	177,0,	0,	0,	0,
	0,	0,	102,255,211,62,	233,255,150,0,
	0,	76,	190,211,102,0,	102,102,0,	0,
	76,	201,24,	150,233,62,	142,0,	0,	0,
	150,102,0,	24,	211,244,76,	0,	0,	0,
	150,211,24,	0,	187,255,102,24,	167,24,
	38,	233,255,255,102,102,255,244,76,	0,
	0,	0,	0,	0,	0,	0,	0,	0,	0,	0,
	0,	0,	0,	0,	0,	0,	0,	0,	0,	0,
	0,	0,	0,	0,	0,	0,	0,	0,	0,	0,

	2, 2, // '
	0,	0,
	150,150,
	150,102,
	139,76,
	0,	0,
	0,	0,
	0,	0,
	0,	0,
	0,	0,
	0,	0,
	0,	0,
	0,	0,
	0,	0,

	4, 3, // (
	0,	0,	0,	0,
	0,	0,	76,	177,
	0,	76,	201,24,
	24,	190,38,	0,
	102,187,0,	0,
	150,150,0,	0,
	150,150,0,	0,
	150,150,0,	0,
	102,187,0,	0,
	24,	190,38,	0,
	0,	76,	223,38,
	0,	0,	76,	177,
	0,	0,	0,	0,

	4, 3, // )
	0,	0,	0,	0,
	177,76,	0,	0,
	38,	223,76,	0,
	0,	38,	190,24,
	0,	0,	187,102,
	0,	0,	150,150,
	0,	0,	150,150,
	0,	0,	150,150,
	0,	0,	187,102,
	0,	38,	190,24,
	24,	201,76,	0,
	177,76,	0,	0,
	0,	0,	0,	0,

	6, 6, // *
	0,	0,	0,	0,	0,	0,
	0,	0,	102,150,0,	0,
	76,	234,167,141,223,76,
	0,	24,	211,233,38,	0,
	76,	234,167,141,223,76,
	0,	0,	150,150,0,	0,
	0,	0,	0,	0,	0,	0,
	0,	0,	0,	0,	0,	0,
	0,	0,	0,	0,	0,	0,
	0,	0,	0,	0,	0,	0,
	0,	0,	0,	0,	0,	0,
	0,	0,	0,	0,	0,	0,
	0,	0,	0,	0,	0,	0,

	8, 8, // +
	0,	0,	0,	0,	0,	0,	0,	0,
	0,	0,	0,	0,	0,	0,	0,	0,
	0,	0,	0,	24,	142,0,	0,	0,
	0,	0,	0,	24,	142,0,	0,	0,
	0,	0,	0,	24,	142,0,	0,	0,
	24,	211,255,255,255,255,255,150,
	0,	0,	0,	24,	142,0,	0,	0,
	0,	0,	0,	24,	142,0,	0,	0,
	0,	0,	0,	24,	142,0,	0,	0,
	0,	0,	0,	0,	0,	0,	0,	0,
	0,	0,	0,	0,	0,	0,	0,	0,
	0,	0,	0,	0,	0,	0,	0,	0,
	0,	0,	0,	0,	0,	0,	0,	0,

	3, 3, // ,
	0,	0,	0,
	0,	0,	0,
	0,	0,	0,
	0,	0,	0,
	0,	0,	0,
	0,	0,	0,
	0,	0,	0,
	0,	0,	0,
	0,	0,	0,
	102,255,102,
	0,	76,	90,
	76,	201,24,
	0,	0,	0,

	4, 4, // -
	0,	0,	0,	0,
	0,	0,	0,	0,
	0,	0,	0,	0,
	0,	0,	0,	0,
	0,	0,	0,	0,
	0,	0,	0,	0,
	0,	0,	0,	0,
	150,255,255,150,
	0,	0,	0,	0,
	0,	0,	0,	0,
	0,	0,	0,	0,
	0,	0,	0,	0,
	0,	0,	0,	0,

	3, 3, // .
	0,	0,	0,
	0,	0,	0,
	0,	0,	0,
	0,	0,	0,
	0,	0,	0,
	0,	0,	0,
	0,	0,	0,
	0,	0,	0,
	0,	0,	0,
	76,	234,76,
	0,	0,	0,
	0,	0,	0,
	0,	0,	0,

	5, 2, // /
	0,	0,	0,	0,	0,
	0,	0,	0,	90,	76,
	0,	0,	0,	142,24,
	0,	0,	38,	128,0,
	0,	0,	62,	62,	0,
	0,	0,	101,24,	0,
	0,	24,	142,0,	0,
	0,	76,	90,	0,	0,
	0,	128,38,	0,	0,
	24,	142,0,	0,	0,
	0,	0,	0,	0,	0,
	0,	0,	0,	0,	0,
	0,	0,	0,	0,	0,

	6, 6, // 0
	0,	0,	0,	0,	0,	0,
	0,	76,	244,244,76,	0,
	24,	201,76,	38,	190,24,
	102,187,0,	0,	187,102,
	150,150,0,	0,	150,150,
	150,150,0,	0,	150,150,
	150,150,0,	0,	150,150,
	102,187,0,	0,	187,102,
	24,	201,76,	76,	201,24,
	0,	76,	244,244,76,	0,
	0,	0,	0,	0,	0,	0,
	0,	0,	0,	0,	0,	0,
	0,	0,	0,	0,	0,	0,

	4, 4, // 1
	0,	0,	0,	0,
	0,	187,150,0,
	150,255,150,0,
	0,	150,150,0,
	0,	150,150,0,
	0,	150,150,0,
	0,	150,150,0,
	0,	150,150,0,
	0,	150,150,0,
	150,255,255,150,
	0,	0,	0,	0,
	0,	0,	0,	0,
	0,	0,	0,	0,

	6, 6, // 2
	0,	0,	0,	0,	0,	0,
	24,	211,255,233,38,	0,
	139,76,	0,	187,211,24,
	0,	0,	0,	76,	223,38,
	0,	0,	0,	38,	212,38,
	0,	0,	0,	76,	139,0,
	0,	0,	0,	142,24,	0,
	0,	0,	150,102,0,	0,
	0,	150,150,0,	38,	128,
	187,255,255,255,244,76,
	0,	0,	0,	0,	0,	0,
	0,	0,	0,	0,	0,	0,
	0,	0,	0,	0,	0,	0,

	6, 5, // 3
	0,	0,	0,	0,	0,	0,
	0,	150,255,244,76,	0,
	76,	139,0,	150,187,0,
	0,	0,	0,	102,150,0,
	0,	0,	24,	142,0,	0,
	0,	76,	244,255,150,0,
	0,	0,	0,	150,211,24,
	0,	0,	0,	38,	190,24,
	0,	0,	0,	102,150,0,
	150,255,255,187,0,	0,
	0,	0,	0,	0,	0,	0,
	0,	0,	0,	0,	0,	0,
	0,	0,	0,	0,	0,	0,

	7, 7, // 4
	0,	0,	0,	0,	0,	0,	0,
	0,	0,	0,	0,	150,150,0,
	0,	0,	0,	38,	233,150,0,
	0,	0,	0,	166,178,150,0,
	0,	0,	76,	90,	150,150,0,
	0,	24,	167,24,	150,150,0,
	0,	90,	76,	0,	150,150,0,
	24,	211,255,255,255,255,150,
	0,	0,	0,	0,	150,150,0,
	0,	0,	0,	0,	150,150,0,
	0,	0,	0,	0,	0,	0,	0,
	0,	0,	0,	0,	0,	0,	0,
	0,	0,	0,	0,	0,	0,	0,

	6, 5, // 5
	0,	0,	0,	0,	0,	0,
	0,	38,	233,255,233,38,
	0,	90,	76,	0,	0,	0,
	24,	211,255,102,0,	0,
	24,	48,	76,	244,102,0,
	0,	0,	0,	76,	201,24,
	0,	0,	0,	24,	190,38,
	0,	0,	0,	24,	167,24,
	0,	0,	0,	102,150,0,
	102,255,255,187,0,	0,
	0,	0,	0,	0,	0,	0,
	0,	0,	0,	0,	0,	0,
	0,	0,	0,	0,	0,	0,

	6, 6, // 6
	0,	0,	0,	0,	0,	0,
	0,	0,	24,	211,255,102,
	0,	38,	233,187,0,	0,
	0,	187,150,0,	0,	0,
	76,	223,212,255,187,0,
	150,212,77,	38,	233,102,
	150,150,0,	0,	187,150,
	102,187,0,	0,	150,150,
	38,	223,76,	24,	201,76,
	0,	102,255,255,102,0,
	0,	0,	0,	0,	0,	0,
	0,	0,	0,	0,	0,	0,
	0,	0,	0,	0,	0,	0,

	6, 6, // 7
	0,	0,	0,	0,	0,	0,
	38,	233,255,255,244,76,
	77,	38,	0,	0,	166,38,
	0,	0,	0,	24,	167,24,
	0,	0,	0,	76,	139,0,
	0,	0,	0,	139,76,	0,
	0,	0,	24,	167,24,	0,
	0,	0,	38,	166,0,	0,
	0,	0,	102,102,0,	0,
	0,	0,	166,38,	0,	0,
	0,	0,	0,	0,	0,	0,
	0,	0,	0,	0,	0,	0,
	0,	0,	0,	0,	0,	0,

	6, 6, // 8
	0,	0,	0,	0,	0,	0,
	0,	102,255,255,102,0,
	38,	223,76,	38,	212,38,
	76,	201,24,	24,	190,38,
	24,	211,150,76,	177,0,
	0,	76,	244,211,24,	0,
	0,	90,	114,233,187,0,
	76,	201,24,	24,	201,76,
	76,	223,38,	24,	201,76,
	0,	150,255,255,150,0,
	0,	0,	0,	0,	0,	0,
	0,	0,	0,	0,	0,	0,
	0,	0,	0,	0,	0,	0,

	6, 6, // 9
	0,	0,	0,	0,	0,	0,
	0,	102,255,255,102,0,
	76,	201,24,	76,	223,38,
	150,150,0,	0,	187,102,
	150,150,0,	0,	150,150,
	102,233,38,	0,	187,150,
	0,	187,255,255,244,76,
	0,	0,	0,	76,	177,0,
	0,	0,	102,233,38,	0,
	102,255,211,24,	0,	0,
	0,	0,	0,	0,	0,	0,
	0,	0,	0,	0,	0,	0,
	0,	0,	0,	0,	0,	0,

	3, 3, // :
	0,	0,	0,
	0,	0,	0,
	0,	0,	0,
	0,	0,	0,
	38,	233,102,
	0,	0,	0,
	0,	0,	0,
	0,	0,	0,
	0,	0,	0,
	38,	233,102,
	0,	0,	0,
	0,	0,	0,
	0,	0,	0,

	3, 3, // ;
	0,	0,	0,
	0,	0,	0,
	0,	0,	0,
	0,	0,	0,
	38,	233,102,
	0,	0,	0,
	0,	0,	0,
	0,	0,	0,
	0,	0,	0,
	76,	244,150,
	0,	24,	142,
	38,	223,76,
	0,	0,	0,

	8, 5, // <
	0,	0,	0,	0,	0,	0,	0,	0,
	0,	0,	0,	0,	0,	0,	0,	0,
	0,	0,	0,	0,	0,	0,	0,	0,
	0,	0,	0,	0,	0,	38,	233,187,
	0,	0,	76,	244,255,233,38,	0,
	24,	211,211,24,	0,	0,	0,	0,
	0,	0,	76,	244,150,0,	0,	0,
	0,	0,	0,	0,	150,255,255,187,
	0,	0,	0,	0,	0,	0,	0,	0,
	0,	0,	0,	0,	0,	0,	0,	0,
	0,	0,	0,	0,	0,	0,	0,	0,
	0,	0,	0,	0,	0,	0,	0,	0,
	0,	0,	0,	0,	0,	0,	0,	0,

	8, 8, // =
	0,	0,	0,	0,	0,	0,	0,	0,
	0,	0,	0,	0,	0,	0,	0,	0,
	0,	0,	0,	0,	0,	0,	0,	0,
	0,	0,	0,	0,	0,	0,	0,	0,
	24,	211,255,255,255,255,255,150,
	0,	0,	0,	0,	0,	0,	0,	0,
	24,	211,255,255,255,255,255,150,
	0,	0,	0,	0,	0,	0,	0,	0,
	0,	0,	0,	0,	0,	0,	0,	0,
	0,	0,	0,	0,	0,	0,	0,	0,
	0,	0,	0,	0,	0,	0,	0,	0,
	0,	0,	0,	0,	0,	0,	0,	0,
	0,	0,	0,	0,	0,	0,	0,	0,

	8, 5, // >
	0,	0,	0,	0,	0,	0,	0,	0,
	0,	0,	0,	0,	0,	0,	0,	0,
	0,	0,	0,	0,	0,	0,	0,	0,
	24,	211,187,0,	0,	0,	0,	0,
	0,	0,	102,255,255,211,24,	0,
	0,	0,	0,	0,	0,	76,	244,150,
	0,	0,	0,	0,	187,211,24,	0,
	24,	211,255,255,102,0,	0,	0,
	0,	0,	0,	0,	0,	0,	0,	0,
	0,	0,	0,	0,	0,	0,	0,	0,
	0,	0,	0,	0,	0,	0,	0,	0,
	0,	0,	0,	0,	0,	0,	0,	0,
	0,	0,	0,	0,	0,	0,	0,	0,

	6, 6, // ?
	0,	0,	0,	0,	0,	0,
	24,	211,255,244,76,	0,
	102,102,0,	102,211,24,
	102,150,0,	38,	190,24,
	0,	0,	0,	102,150,0,
	0,	0,	24,	167,24,	0,
	0,	0,	90,	76,	0,	0,
	0,	0,	101,24,	0,	0,
	0,	0,	72,	0,	0,	0,
	0,	38,	233,102,0,	0,
	0,	0,	0,	0,	0,	0,
	0,	0,	0,	0,	0,	0,
	0,	0,	0,	0,	0,	0,

	11, 11, // @
	0,	0,	0,	0,	0,	0,	0,	0,	0,	0,	0,
	0,	0,	0,	38,	233,255,255,244,76,	0,	0,
	0,	0,	150,233,38,	0,	0,	24,	211,102,0,
	0,	102,150,0,	38,	233,211,48,	156,190,38,
	24,	167,24,	24,	211,102,76,	244,102,90,	76,
	76,	90,	0,	150,187,0,	38,	212,38,	76,	90,
	90,	76,	24,	201,76,	0,	102,211,24,	90,	76,
	128,38,	76,	201,24,	0,	187,150,0,	166,38,
	90,	76,	76,	201,24,	150,244,76,	102,150,0,
	38,	128,24,	211,255,165,211,255,187,24,	142,
	0,	177,76,	0,	0,	0,	0,	0,	0,	177,76,
	0,	24,	211,150,0,	0,	0,	38,	233,102,0,
	0,	0,	0,	150,255,255,255,233,38,	0,	0,

	10, 10, // A
	0,	0,	0,	0,	0,	0,	0,	0,	0,	0,
	0,	0,	0,	0,	38,	166,0,	0,	0,	0,
	0,	0,	0,	0,	150,233,38,	0,	0,	0,
	0,	0,	0,	24,	179,223,150,0,	0,	0,
	0,	0,	0,	102,150,150,211,24,	0,	0,
	0,	0,	0,	166,38,	38,	233,102,0,	0,
	0,	0,	76,	244,255,255,255,187,0,	0,
	0,	0,	139,76,	0,	0,	102,244,76,	0,
	0,	38,	190,24,	0,	0,	24,	211,187,0,
	24,	211,255,187,0,	0,	150,255,255,150,
	0,	0,	0,	0,	0,	0,	0,	0,	0,	0,
	0,	0,	0,	0,	0,	0,	0,	0,	0,	0,
	0,	0,	0,	0,	0,	0,	0,	0,	0,	0,

	9, 8, // B
	0,	0,	0,	0,	0,	0,	0,	0,	0,
	24,	211,255,255,255,255,211,24,	0,
	0,	24,	211,102,0,	0,	187,187,0,
	0,	24,	211,102,0,	0,	76,	201,24,
	0,	24,	211,102,0,	0,	187,187,0,
	0,	24,	211,255,255,255,187,0,	0,
	0,	24,	211,102,0,	0,	187,233,38,
	0,	24,	211,102,0,	0,	76,	234,76,
	0,	24,	211,102,0,	0,	187,233,38,
	24,	211,255,255,255,255,255,102,0,
	0,	0,	0,	0,	0,	0,	0,	0,	0,
	0,	0,	0,	0,	0,	0,	0,	0,	0,
	0,	0,	0,	0,	0,	0,	0,	0,	0,

	8, 8, // C
	0,	0,	0,	0,	0,	0,	0,	0,
	0,	0,	150,255,255,150,139,76,
	0,	187,211,24,	0,	150,244,76,
	76,	234,76,	0,	0,	0,	102,102,
	150,211,24,	0,	0,	0,	0,	0,
	150,211,24,	0,	0,	0,	0,	0,
	150,211,24,	0,	0,	0,	0,	0,
	76,	234,76,	0,	0,	0,	76,	90,
	0,	187,211,24,	0,	38,	190,24,
	0,	0,	187,255,255,233,38,	0,
	0,	0,	0,	0,	0,	0,	0,	0,
	0,	0,	0,	0,	0,	0,	0,	0,
	0,	0,	0,	0,	0,	0,	0,	0,

	10, 9, // D
	0,	0,	0,	0,	0,	0,	0,	0,	0,	0,
	24,	211,255,255,255,255,255,102,0,	0,
	0,	24,	211,102,0,	0,	150,255,102,0,
	0,	24,	211,102,0,	0,	0,	150,211,24,
	0,	24,	211,102,0,	0,	0,	76,	234,76,
	0,	24,	211,102,0,	0,	0,	38,	223,76,
	0,	24,	211,102,0,	0,	0,	76,	234,76,
	0,	24,	211,102,0,	0,	0,	150,211,24,
	0,	24,	211,102,0,	0,	150,255,102,0,
	24,	211,255,255,255,255,244,76,	0,	0,
	0,	0,	0,	0,	0,	0,	0,	0,	0,	0,
	0,	0,	0,	0,	0,	0,	0,	0,	0,	0,
	0,	0,	0,	0,	0,	0,	0,	0,	0,	0,

	9, 8, // E
	0,	0,	0,	0,	0,	0,	0,	0,	0,
	24,	211,255,255,255,255,255,102,0,
	0,	24,	211,102,0,	0,	102,102,0,
	0,	24,	211,102,0,	0,	0,	0,	0,
	0,	24,	211,102,0,	102,102,0,	0,
	0,	24,	211,255,255,255,102,0,	0,
	0,	24,	211,102,0,	102,102,0,	0,
	0,	24,	211,102,0,	0,	0,	0,	0,
	0,	24,	211,102,0,	0,	38,	190,24,
	24,	211,255,255,255,255,255,150,0,
	0,	0,	0,	0,	0,	0,	0,	0,	0,
	0,	0,	0,	0,	0,	0,	0,	0,	0,
	0,	0,	0,	0,	0,	0,	0,	0,	0,

	8, 8, // F
	0,	0,	0,	0,	0,	0,	0,	0,
	24,	211,255,255,255,255,244,76,
	0,	24,	211,102,0,	0,	90,	76,
	0,	24,	211,102,0,	0,	0,	0,
	0,	24,	211,102,0,	166,38,	0,
	0,	24,	211,255,255,233,38,	0,
	0,	24,	211,102,0,	166,38,	0,
	0,	24,	211,102,0,	0,	0,	0,
	0,	24,	211,102,0,	0,	0,	0,
	24,	211,255,255,102,0,	0,	0,
	0,	0,	0,	0,	0,	0,	0,	0,
	0,	0,	0,	0,	0,	0,	0,	0,
	0,	0,	0,	0,	0,	0,	0,	0,

	9, 9, // G
	0,	0,	0,	0,	0,	0,	0,	0,	0,
	0,	0,	150,255,255,187,102,102,0,
	0,	150,233,38,	0,	102,255,102,0,
	76,	234,76,	0,	0,	0,	76,	90,	0,
	150,211,24,	0,	0,	0,	0,	0,	0,
	150,211,24,	0,	0,	187,255,255,102,
	150,233,38,	0,	0,	0,	150,150,0,
	76,	244,102,0,	0,	0,	150,150,0,
	0,	187,244,76,	0,	0,	150,150,0,
	0,	0,	150,255,255,255,211,24,	0,
	0,	0,	0,	0,	0,	0,	0,	0,	0,
	0,	0,	0,	0,	0,	0,	0,	0,	0,
	0,	0,	0,	0,	0,	0,	0,	0,	0,

	10, 10, // H
	0,	0,	0,	0,	0,	0,	0,	0,	0,	0,
	24,	211,255,255,187,38,	233,255,255,150,
	0,	0,	187,150,0,	0,	24,	211,102,0,
	0,	0,	187,150,0,	0,	24,	211,102,0,
	0,	0,	187,150,0,	0,	24,	211,102,0,
	0,	0,	187,255,255,255,255,255,102,0,
	0,	0,	187,150,0,	0,	24,	211,102,0,
	0,	0,	187,150,0,	0,	24,	211,102,0,
	0,	0,	187,150,0,	0,	24,	211,102,0,
	24,	211,255,255,187,38,	233,255,255,150,
	0,	0,	0,	0,	0,	0,	0,	0,	0,	0,
	0,	0,	0,	0,	0,	0,	0,	0,	0,	0,
	0,	0,	0,	0,	0,	0,	0,	0,	0,	0,

	4, 4, // I
	0,	0,	0,	0,
	187,255,255,150,
	0,	150,150,0,
	0,	150,150,0,
	0,	150,150,0,
	0,	150,150,0,
	0,	150,150,0,
	0,	150,150,0,
	0,	150,150,0,
	187,255,255,150,
	0,	0,	0,	0,
	0,	0,	0,	0,
	0,	0,	0,	0,

	5, 4, // J
	0,	0,	0,	0,	0,
	0,	187,255,255,187,
	0,	0,	150,150,0,
	0,	0,	150,150,0,
	0,	0,	150,150,0,
	0,	0,	150,150,0,
	0,	0,	150,150,0,
	0,	0,	150,150,0,
	187,150,150,102,0,
	150,255,187,0,	0,
	0,	0,	0,	0,	0,
	0,	0,	0,	0,	0,
	0,	0,	0,	0,	0,

	10, 10, // K
	0,	0,	0,	0,	0,	0,	0,	0,	0,	0,
	24,	211,255,255,102,150,255,255,187,0,
	0,	24,	211,102,0,	24,	211,150,0,	0,
	0,	24,	211,102,24,	201,76,	0,	0,	0,
	0,	24,	211,167,234,76,	0,	0,	0,	0,
	0,	24,	211,255,255,102,0,	0,	0,	0,
	0,	24,	211,102,102,244,76,	0,	0,	0,
	0,	24,	211,102,0,	102,244,76,	0,	0,
	0,	24,	211,102,0,	0,	187,244,76,	0,
	24,	211,255,255,102,76,	244,255,255,150,
	0,	0,	0,	0,	0,	0,	0,	0,	0,	0,
	0,	0,	0,	0,	0,	0,	0,	0,	0,	0,
	0,	0,	0,	0,	0,	0,	0,	0,	0,	0,

	8, 8, // L
	0,	0,	0,	0,	0,	0,	0,	0,
	24,	211,255,255,102,0,	0,	0,
	0,	24,	211,102,0,	0,	0,	0,
	0,	24,	211,102,0,	0,	0,	0,
	0,	24,	211,102,0,	0,	0,	0,
	0,	24,	211,102,0,	0,	0,	0,
	0,	24,	211,102,0,	0,	0,	0,
	0,	24,	211,102,0,	0,	0,	0,
	0,	24,	211,102,0,	0,	76,	177,
	24,	211,255,255,255,255,255,102,
	0,	0,	0,	0,	0,	0,	0,	0,
	0,	0,	0,	0,	0,	0,	0,	0,
	0,	0,	0,	0,	0,	0,	0,	0,

	12, 12, // M
	0,	0,	0,	0,	0,	0,	0,	0,	0,	0,	0,	0,
	24,	211,255,102,0,	0,	0,	0,	24,	211,255,150,
	0,	24,	211,211,24,	0,	0,	0,	76,	213,150,0,
	0,	24,	179,213,76,	0,	0,	0,	128,178,150,0,
	0,	24,	167,126,187,0,	0,	38,	166,150,150,0,
	0,	24,	167,62,	212,38,	0,	90,	76,	150,150,0,
	0,	24,	167,24,	150,150,0,	142,24,	150,150,0,
	0,	24,	167,24,	38,	190,100,139,0,	150,150,0,
	0,	24,	167,24,	0,	187,201,38,	0,	150,150,0,
	24,	211,255,211,24,	76,	177,0,	187,255,255,150,
	0,	0,	0,	0,	0,	0,	0,	0,	0,	0,	0,	0,
	0,	0,	0,	0,	0,	0,	0,	0,	0,	0,	0,	0,
	0,	0,	0,	0,	0,	0,	0,	0,	0,	0,	0,	0,

	10, 10, // N
	0,	0,	0,	0,	0,	0,	0,	0,	0,	0,
	76,	244,255,150,0,	0,	76,	244,255,150,
	0,	0,	150,244,76,	0,	0,	38,	128,0,
	0,	0,	128,178,211,24,	0,	38,	128,0,
	0,	0,	128,62,	211,150,0,	38,	128,0,
	0,	0,	128,38,	38,	233,102,38,	128,0,
	0,	0,	128,38,	0,	102,233,77,	128,0,
	0,	0,	128,38,	0,	0,	187,212,128,0,
	0,	0,	128,38,	0,	0,	38,	233,150,0,
	0,	187,255,233,38,	0,	0,	76,	139,0,
	0,	0,	0,	0,	0,	0,	0,	0,	0,	0,
	0,	0,	0,	0,	0,	0,	0,	0,	0,	0,
	0,	0,	0,	0,	0,	0,	0,	0,	0,	0,

	9, 9, // O
	0,	0,	0,	0,	0,	0,	0,	0,	0,
	0,	0,	150,255,255,244,76,	0,	0,
	0,	187,211,24,	0,	76,	244,102,0,
	76,	234,76,	0,	0,	0,	150,211,24,
	150,211,24,	0,	0,	0,	76,	234,76,
	150,211,24,	0,	0,	0,	76,	234,76,
	150,211,24,	0,	0,	0,	76,	234,76,
	76,	234,76,	0,	0,	0,	150,211,24,
	0,	187,211,24,	0,	76,	244,102,0,
	0,	0,	150,255,255,244,76,	0,	0,
	0,	0,	0,	0,	0,	0,	0,	0,	0,
	0,	0,	0,	0,	0,	0,	0,	0,	0,
	0,	0,	0,	0,	0,	0,	0,	0,	0,

	8, 7, // P
	0,	0,	0,	0,	0,	0,	0,	0,
	24,	211,255,255,255,255,102,0,
	0,	24,	211,102,0,	102,244,76,
	0,	24,	211,102,0,	24,	211,150,
	0,	24,	211,102,0,	102,255,102,
	0,	24,	211,255,255,255,150,0,
	0,	24,	211,102,0,	0,	0,	0,
	0,	24,	211,102,0,	0,	0,	0,
	0,	24,	211,102,0,	0,	0,	0,
	24,	211,255,255,102,0,	0,	0,
	0,	0,	0,	0,	0,	0,	0,	0,
	0,	0,	0,	0,	0,	0,	0,	0,
	0,	0,	0,	0,	0,	0,	0,	0,

	9, 9, // Q
	0,	0,	0,	0,	0,	0,	0,	0,	0,
	0,	0,	150,255,255,244,76,	0,	0,
	0,	187,211,24,	0,	76,	244,102,0,
	76,	234,76,	0,	0,	0,	150,211,24,
	150,211,24,	0,	0,	0,	76,	234,76,
	150,211,24,	0,	0,	0,	38,	223,76,
	150,211,24,	0,	0,	0,	76,	234,76,
	76,	234,76,	0,	0,	0,	102,211,24,
	0,	187,211,24,	0,	76,	244,102,0,
	0,	0,	187,255,255,255,102,0,	0,
	0,	0,	0,	38,	233,233,38,	0,	0,
	0,	0,	0,	0,	0,	76,	244,244,76,
	0,	0,	0,	0,	0,	0,	0,	0,	0,

	10, 10, // R
	0,	0,	0,	0,	0,	0,	0,	0,	0,	0,
	24,	211,255,255,255,255,150,0,	0,	0,
	0,	24,	211,102,0,	102,255,102,0,	0,
	0,	24,	211,102,0,	24,	211,150,0,	0,
	0,	24,	211,102,0,	150,244,76,	0,	0,
	0,	24,	211,255,255,211,24,	0,	0,	0,
	0,	24,	211,102,102,187,0,	0,	0,	0,
	0,	24,	211,102,0,	187,102,0,	0,	0,
	0,	24,	211,102,0,	38,	212,38,	0,	0,
	24,	211,255,255,102,0,	102,255,233,38,
	0,	0,	0,	0,	0,	0,	0,	0,	0,	0,
	0,	0,	0,	0,	0,	0,	0,	0,	0,	0,
	0,	0,	0,	0,	0,	0,	0,	0,	0,	0,

	7, 7, // S
	0,	0,	0,	0,	0,	0,	0,
	0,	150,255,233,114,139,0,
	76,	223,38,	38,	233,150,0,
	76,	201,24,	0,	76,	139,0,
	24,	211,233,38,	0,	0,	0,
	0,	24,	211,255,187,0,	0,
	0,	0,	0,	38,	233,187,0,
	76,	139,0,	0,	76,	223,38,
	76,	223,38,	0,	102,211,24,
	76,	129,233,255,233,38,	0,
	0,	0,	0,	0,	0,	0,	0,
	0,	0,	0,	0,	0,	0,	0,
	0,	0,	0,	0,	0,	0,	0,

	8, 8, // T
	0,	0,	0,	0,	0,	0,	0,	0,
	150,255,255,255,255,255,211,24,
	177,76,	24,	201,76,	24,	167,24,
	0,	0,	24,	201,76,	0,	0,	0,
	0,	0,	24,	201,76,	0,	0,	0,
	0,	0,	24,	201,76,	0,	0,	0,
	0,	0,	24,	201,76,	0,	0,	0,
	0,	0,	24,	201,76,	0,	0,	0,
	0,	0,	24,	201,76,	0,	0,	0,
	0,	38,	233,255,255,102,0,	0,
	0,	0,	0,	0,	0,	0,	0,	0,
	0,	0,	0,	0,	0,	0,	0,	0,
	0,	0,	0,	0,	0,	0,	0,	0,

	10, 10, // U
	0,	0,	0,	0,	0,	0,	0,	0,	0,	0,
	24,	211,255,255,102,0,	76,	244,255,150,
	0,	24,	211,102,0,	0,	0,	38,	128,0,
	0,	24,	211,102,0,	0,	0,	38,	128,0,
	0,	24,	211,102,0,	0,	0,	38,	128,0,
	0,	24,	211,102,0,	0,	0,	38,	128,0,
	0,	0,	187,102,0,	0,	0,	76,	139,0,
	0,	0,	187,102,0,	0,	0,	76,	90,	0,
	0,	0,	102,233,38,	0,	38,	212,38,	0,
	0,	0,	0,	150,255,255,244,76,	0,	0,
	0,	0,	0,	0,	0,	0,	0,	0,	0,	0,
	0,	0,	0,	0,	0,	0,	0,	0,	0,	0,
	0,	0,	0,	0,	0,	0,	0,	0,	0,	0,

	10, 10, // V
	0,	0,	0,	0,	0,	0,	0,	0,	0,	0,
	24,	211,255,255,102,0,	38,	233,255,150,
	0,	38,	233,150,0,	0,	0,	76,	177,0,
	0,	0,	150,211,24,	0,	0,	90,	76,	0,
	0,	0,	38,	223,76,	0,	24,	167,24,	0,
	0,	0,	0,	187,187,0,	76,	90,	0,	0,
	0,	0,	0,	76,	234,76,	166,38,	0,	0,
	0,	0,	0,	0,	187,178,128,0,	0,	0,
	0,	0,	0,	0,	102,244,76,	0,	0,	0,
	0,	0,	0,	0,	24,	142,0,	0,	0,	0,
	0,	0,	0,	0,	0,	0,	0,	0,	0,	0,
	0,	0,	0,	0,	0,	0,	0,	0,	0,	0,
	0,	0,	0,	0,	0,	0,	0,	0,	0,	0,

	13, 13, // W
	0,	0,	0,	0,	0,	0,	0,	0,	0,	0,	0,	0,	0,
	24,	211,255,244,114,233,255,233,38,	76,	244,255,102,
	0,	38,	233,102,0,	38,	223,76,	0,	0,	102,102,0,
	0,	0,	150,187,0,	0,	150,150,0,	0,	128,38,	0,
	0,	0,	76,	223,38,	24,	190,201,24,	24,	142,0,	0,
	0,	0,	24,	211,102,76,	114,201,76,	76,	90,	0,	0,
	0,	0,	0,	150,187,128,38,	150,150,128,38,	0,	0,
	0,	0,	0,	76,	234,190,24,	76,	224,177,0,	0,	0,
	0,	0,	0,	24,	211,150,0,	38,	233,102,0,	0,	0,
	0,	0,	0,	0,	139,76,	0,	0,	166,38,	0,	0,	0,
	0,	0,	0,	0,	0,	0,	0,	0,	0,	0,	0,	0,	0,
	0,	0,	0,	0,	0,	0,	0,	0,	0,	0,	0,	0,	0,
	0,	0,	0,	0,	0,	0,	0,	0,	0,	0,	0,	0,	0,

	10, 10, // X
	0,	0,	0,	0,	0,	0,	0,	0,	0,	0,
	0,	187,255,255,233,38,	150,255,255,102,
	0,	0,	150,244,76,	0,	0,	177,76,	0,
	0,	0,	0,	150,211,24,	150,102,0,	0,
	0,	0,	0,	24,	211,255,150,0,	0,	0,
	0,	0,	0,	0,	76,	244,102,0,	0,	0,
	0,	0,	0,	24,	167,165,233,38,	0,	0,
	0,	0,	0,	177,76,	0,	187,187,0,	0,
	0,	0,	187,102,0,	0,	76,	244,150,0,
	24,	211,255,233,38,	24,	211,255,255,150,
	0,	0,	0,	0,	0,	0,	0,	0,	0,	0,
	0,	0,	0,	0,	0,	0,	0,	0,	0,	0,
	0,	0,	0,	0,	0,	0,	0,	0,	0,	0,

	9, 9, // Y
	0,	0,	0,	0,	0,	0,	0,	0,	0,
	150,255,255,187,0,	76,	244,255,150,
	0,	38,	212,38,	0,	0,	139,76,	0,
	0,	0,	102,187,0,	76,	139,0,	0,
	0,	0,	24,	201,76,	142,24,	0,	0,
	0,	0,	0,	102,234,90,	0,	0,	0,
	0,	0,	0,	38,	212,38,	0,	0,	0,
	0,	0,	0,	38,	212,38,	0,	0,	0,
	0,	0,	0,	38,	212,38,	0,	0,	0,
	0,	0,	76,	244,255,244,76,	0,	0,
	0,	0,	0,	0,	0,	0,	0,	0,	0,
	0,	0,	0,	0,	0,	0,	0,	0,	0,
	0,	0,	0,	0,	0,	0,	0,	0,	0,

	9, 8, // Z
	0,	0,	0,	0,	0,	0,	0,	0,	0,
	0,	38,	233,255,255,255,255,233,38,
	0,	38,	166,0,	0,	76,	234,76,	0,
	0,	76,	90,	0,	24,	211,150,0,	0,
	0,	0,	0,	0,	150,211,24,	0,	0,
	0,	0,	0,	76,	234,76,	0,	0,	0,
	0,	0,	24,	211,150,0,	0,	0,	0,
	0,	0,	150,211,24,	0,	0,	166,38,
	0,	76,	234,76,	0,	0,	38,	190,24,
	24,	211,255,255,255,255,255,187,0,
	0,	0,	0,	0,	0,	0,	0,	0,	0,
	0,	0,	0,	0,	0,	0,	0,	0,	0,
	0,	0,	0,	0,	0,	0,	0,	0,	0,

	4, 4, // [
	0,	0,	0,	0,
	38,	233,255,187,
	38,	212,38,	0,
	38,	212,38,	0,
	38,	212,38,	0,
	38,	212,38,	0,
	38,	212,38,	0,
	38,	212,38,	0,
	38,	212,38,	0,
	38,	212,38,	0,
	38,	212,38,	0,
	38,	233,255,187,
	0,	0,	0,	0,

	5, 2, // backslash
	0,	0,	0,	0,	0,
	24,	142,0,	0,	0,
	0,	128,38,	0,	0,
	0,	76,	90,	0,	0,
	0,	24,	142,0,	0,
	0,	0,	101,24,	0,
	0,	0,	62,	62,	0,
	0,	0,	38,	128,0,
	0,	0,	0,	142,24,
	0,	0,	0,	90,	76,
	0,	0,	0,	0,	0,
	0,	0,	0,	0,	0,
	0,	0,	0,	0,	0,

	4, 4, // ]
	0,	0,	0,	0,
	187,255,233,38,
	0,	38,	212,38,
	0,	38,	212,38,
	0,	38,	212,38,
	0,	38,	212,38,
	0,	38,	212,38,
	0,	38,	212,38,
	0,	38,	212,38,
	0,	38,	212,38,
	0,	38,	212,38,
	187,255,233,38,
	0,	0,	0,	0,

	6, 6, // ^
	0,	0,	0,	0,	0,	0,
	0,	24,	211,187,0,	0,
	0,	90,	76,	102,102,0,
	38,	166,0,	0,	142,24,
	166,38,	0,	0,	76,	139,
	0,	0,	0,	0,	0,	0,
	0,	0,	0,	0,	0,	0,
	0,	0,	0,	0,	0,	0,
	0,	0,	0,	0,	0,	0,
	0,	0,	0,	0,	0,	0,
	0,	0,	0,	0,	0,	0,
	0,	0,	0,	0,	0,	0,
	0,	0,	0,	0,	0,	0,

	8, 8, // _
	0,	0,	0,	0,	0,	0,	0,	0,
	0,	0,	0,	0,	0,	0,	0,	0,
	0,	0,	0,	0,	0,	0,	0,	0,
	0,	0,	0,	0,	0,	0,	0,	0,
	0,	0,	0,	0,	0,	0,	0,	0,
	0,	0,	0,	0,	0,	0,	0,	0,
	0,	0,	0,	0,	0,	0,	0,	0,
	0,	0,	0,	0,	0,	0,	0,	0,
	0,	0,	0,	0,	0,	0,	0,	0,
	0,	0,	0,	0,	0,	0,	0,	0,
	0,	0,	0,	0,	0,	0,	0,	0,
	0,	0,	0,	0,	0,	0,	0,	0,
	76,	244,255,255,255,255,244,76,

	3, 3, // `
	0,	0,	0,
	0,	0,	0,
	76,	234,76,
	0,	38,	77,
	0,	0,	0,
	0,	0,	0,
	0,	0,	0,
	0,	0,	0,
	0,	0,	0,
	0,	0,	0,
	0,	0,	0,
	0,	0,	0,
	0,	0,	0,

	6, 6, // a
	0,	0,	0,	0,	0,	0,
	0,	0,	0,	0,	0,	0,
	0,	0,	0,	0,	0,	0,
	0,	0,	0,	0,	0,	0,
	38,	233,255,244,76,	0,
	102,187,0,	187,150,0,
	0,	0,	0,	150,150,0,
	0,	187,255,255,150,0,
	150,150,0,	150,150,0,
	102,255,255,255,255,102,
	0,	0,	0,	0,	0,	0,
	0,	0,	0,	0,	0,	0,
	0,	0,	0,	0,	0,	0,

	7, 6, // b
	0,	0,	0,	0,	0,	0,	0,
	38,	233,211,24,	0,	0,	0,
	0,	76,	201,24,	0,	0,	0,
	0,	76,	201,24,	0,	0,	0,
	0,	76,	201,200,255,187,0,
	0,	76,	244,102,24,	211,102,
	0,	76,	201,24,	0,	150,150,
	0,	76,	201,24,	0,	150,150,
	0,	76,	201,24,	38,	223,76,
	0,	24,	211,255,255,102,0,
	0,	0,	0,	0,	0,	0,	0,
	0,	0,	0,	0,	0,	0,	0,
	0,	0,	0,	0,	0,	0,	0,

	6, 6, // c
	0,	0,	0,	0,	0,	0,
	0,	0,	0,	0,	0,	0,
	0,	0,	0,	0,	0,	0,
	0,	0,	0,	0,	0,	0,
	0,	150,255,255,102,0,
	102,211,24,	102,211,24,
	150,102,0,	0,	0,	0,
	150,150,0,	0,	0,	0,
	102,233,38,	0,	101,24,
	0,	150,255,255,150,0,
	0,	0,	0,	0,	0,	0,
	0,	0,	0,	0,	0,	0,
	0,	0,	0,	0,	0,	0,

	7, 6, // d
	0,	0,	0,	0,	0,	0,	0,
	0,	0,	0,	187,244,76,	0,
	0,	0,	0,	24,	201,76,	0,
	0,	0,	0,	24,	201,76,	0,
	0,	102,255,255,244,76,	0,
	76,	223,38,	38,	223,76,	0,
	150,150,0,	24,	201,76,	0,
	150,150,0,	24,	201,76,	0,
	102,233,38,	24,	201,76,	0,
	0,	187,255,255,255,233,38,
	0,	0,	0,	0,	0,	0,	0,
	0,	0,	0,	0,	0,	0,	0,
	0,	0,	0,	0,	0,	0,	0,

	6, 6, // e
	0,	0,	0,	0,	0,	0,
	0,	0,	0,	0,	0,	0,
	0,	0,	0,	0,	0,	0,
	0,	0,	0,	0,	0,	0,
	0,	150,255,244,76,	0,
	102,150,0,	102,211,24,
	150,255,255,255,233,38,
	150,102,0,	0,	0,	0,
	102,211,24,	0,	101,24,
	0,	150,255,255,150,0,
	0,	0,	0,	0,	0,	0,
	0,	0,	0,	0,	0,	0,
	0,	0,	0,	0,	0,	0,

	6, 5, // f
	0,	0,	0,	0,	0,	0,
	0,	0,	187,255,244,76,
	0,	102,150,0,	0,	0,
	0,	150,150,0,	0,	0,
	150,255,255,211,24,	0,
	0,	150,150,0,	0,	0,
	0,	150,150,0,	0,	0,
	0,	150,150,0,	0,	0,
	0,	150,150,0,	0,	0,
	150,255,255,211,24,	0,
	0,	0,	0,	0,	0,	0,
	0,	0,	0,	0,	0,	0,
	0,	0,	0,	0,	0,	0,

	6, 6, // g
	0,	0,	0,	0,	0,	0,
	0,	0,	0,	0,	0,	0,
	0,	0,	0,	0,	0,	0,
	0,	0,	0,	0,	0,	0,
	0,	187,255,255,255,187,
	76,	201,24,	76,	223,38,
	38,	212,38,	38,	212,38,
	0,	150,255,255,102,0,
	76,	177,0,	0,	0,	0,
	38,	233,255,255,255,102,
	76,	90,	0,	0,	24,	142,
	187,102,0,	0,	76,	90,
	76,	244,255,255,211,24,

	8, 8, // h
	0,	0,	0,	0,	0,	0,	0,	0,
	38,	233,233,38,	0,	0,	0,	0,
	0,	76,	223,38,	0,	0,	0,	0,
	0,	76,	223,38,	0,	0,	0,	0,
	0,	76,	223,141,255,211,24,	0,
	0,	76,	244,187,24,	201,76,	0,
	0,	76,	223,38,	24,	201,76,	0,
	0,	76,	223,38,	24,	201,76,	0,
	0,	76,	223,38,	24,	201,76,	0,
	24,	211,255,211,200,255,233,38,
	0,	0,	0,	0,	0,	0,	0,	0,
	0,	0,	0,	0,	0,	0,	0,	0,
	0,	0,	0,	0,	0,	0,	0,	0,

	4, 4, // i
	0,	0,	0,	0,
	24,	211,102,0,
	0,	0,	0,	0,
	0,	0,	0,	0,
	24,	211,102,0,
	187,255,102,0,
	24,	211,102,0,
	24,	211,102,0,
	24,	211,102,0,
	102,255,211,24,
	0,	0,	0,	0,
	0,	0,	0,	0,
	0,	0,	0,	0,

	5, 4, // j
	0,	0,	0,	0,	0,
	0,	0,	24,	211,102,
	0,	0,	0,	0,	0,
	0,	0,	0,	0,	0,
	0,	0,	187,255,102,
	0,	0,	24,	211,102,
	0,	0,	24,	211,102,
	0,	0,	24,	211,102,
	0,	0,	24,	211,102,
	0,	0,	24,	211,102,
	0,	0,	24,	201,76,
	0,	0,	24,	190,38,
	38,	233,255,150,0,

	8, 8, // k
	0,	0,	0,	0,	0,	0,	0,	0,
	24,	211,244,76,	0,	0,	0,	0,
	0,	38,	223,76,	0,	0,	0,	0,
	0,	38,	223,76,	0,	0,	0,	0,
	0,	38,	223,76,	150,255,211,24,
	0,	38,	223,76,	102,102,0,	0,
	0,	38,	223,234,187,0,	0,	0,
	0,	38,	233,224,211,24,	0,	0,
	0,	38,	223,76,	187,187,0,	0,
	24,	211,255,233,178,255,244,76,
	0,	0,	0,	0,	0,	0,	0,	0,
	0,	0,	0,	0,	0,	0,	0,	0,
	0,	0,	0,	0,	0,	0,	0,	0,

	5, 5, // l
	0,	0,	0,	0,	0,
	38,	233,233,38,	0,
	0,	76,	223,38,	0,
	0,	76,	223,38,	0,
	0,	76,	223,38,	0,
	0,	76,	223,38,	0,
	0,	76,	223,38,	0,
	0,	76,	223,38,	0,
	0,	76,	223,38,	0,
	24,	211,255,211,24,
	0,	0,	0,	0,	0,
	0,	0,	0,	0,	0,
	0,	0,	0,	0,	0,

	11, 11, // m
	0,	0,	0,	0,	0,	0,	0,	0,	0,	0,	0,
	0,	0,	0,	0,	0,	0,	0,	0,	0,	0,	0,
	0,	0,	0,	0,	0,	0,	0,	0,	0,	0,	0,
	0,	0,	0,	0,	0,	0,	0,	0,	0,	0,	0,
	38,	233,211,200,255,150,150,255,187,0,	0,
	0,	76,	244,102,76,	244,150,76,	223,38,	0,
	0,	76,	223,38,	76,	223,38,	76,	223,38,	0,
	0,	76,	223,38,	76,	223,38,	76,	223,38,	0,
	0,	76,	223,38,	76,	223,38,	76,	223,38,	0,
	24,	211,255,234,244,255,234,244,255,211,24,
	0,	0,	0,	0,	0,	0,	0,	0,	0,	0,	0,
	0,	0,	0,	0,	0,	0,	0,	0,	0,	0,	0,
	0,	0,	0,	0,	0,	0,	0,	0,	0,	0,	0,

	8, 8, // n
	0,	0,	0,	0,	0,	0,	0,	0,
	0,	0,	0,	0,	0,	0,	0,	0,
	0,	0,	0,	0,	0,	0,	0,	0,
	0,	0,	0,	0,	0,	0,	0,	0,
	38,	233,233,141,255,187,0,	0,
	0,	76,	244,187,76,	223,38,	0,
	0,	76,	223,38,	76,	223,38,	0,
	0,	76,	223,38,	76,	223,38,	0,
	0,	76,	223,38,	76,	223,38,	0,
	24,	211,255,234,244,255,211,24,
	0,	0,	0,	0,	0,	0,	0,	0,
	0,	0,	0,	0,	0,	0,	0,	0,
	0,	0,	0,	0,	0,	0,	0,	0,

	6, 6, // o
	0,	0,	0,	0,	0,	0,
	0,	0,	0,	0,	0,	0,
	0,	0,	0,	0,	0,	0,
	0,	0,	0,	0,	0,	0,
	0,	150,255,255,150,0,
	76,	201,24,	76,	244,102,
	150,150,0,	0,	187,150,
	150,187,0,	0,	150,150,
	76,	234,76,	24,	201,76,
	0,	150,255,255,102,0,
	0,	0,	0,	0,	0,	0,
	0,	0,	0,	0,	0,	0,
	0,	0,	0,	0,	0,	0,

	7, 7, // p
	0,	0,	0,	0,	0,	0,	0,
	0,	0,	0,	0,	0,	0,	0,
	0,	0,	0,	0,	0,	0,	0,
	0,	0,	0,	0,	0,	0,	0,
	38,	233,211,165,255,211,24,
	0,	76,	244,150,24,	211,150,
	0,	76,	201,24,	0,	150,187,
	0,	76,	201,24,	0,	150,150,
	0,	76,	201,24,	24,	211,102,
	0,	76,	244,255,255,150,0,
	0,	76,	201,24,	0,	0,	0,
	0,	76,	201,24,	0,	0,	0,
	38,	233,255,211,24,	0,	0,

	7, 6, // q
	0,	0,	0,	0,	0,	0,	0,
	0,	0,	0,	0,	0,	0,	0,
	0,	0,	0,	0,	0,	0,	0,
	0,	0,	0,	0,	0,	0,	0,
	0,	102,255,255,244,76,	0,
	76,	201,24,	38,	223,76,	0,
	150,150,0,	24,	201,76,	0,
	150,150,0,	24,	201,76,	0,
	102,233,38,	24,	201,76,	0,
	0,	187,255,255,244,76,	0,
	0,	0,	0,	24,	201,76,	0,
	0,	0,	0,	24,	201,76,	0,
	0,	0,	0,	187,255,233,38,

	6, 5, // r
	0,	0,	0,	0,	0,	0,
	0,	0,	0,	0,	0,	0,
	0,	0,	0,	0,	0,	0,
	0,	0,	0,	0,	0,	0,
	0,	38,	223,167,233,38,
	24,	211,255,187,0,	0,
	0,	38,	223,76,	0,	0,
	0,	38,	223,76,	0,	0,
	0,	38,	223,76,	0,	0,
	24,	211,255,233,38,	0,
	0,	0,	0,	0,	0,	0,
	0,	0,	0,	0,	0,	0,
	0,	0,	0,	0,	0,	0,

	5, 5, // s
	0,	0,	0,	0,	0,
	0,	0,	0,	0,	0,
	0,	0,	0,	0,	0,
	0,	0,	0,	0,	0,
	38,	233,255,211,24,
	102,102,24,	167,24,
	76,	244,211,24,	0,
	0,	0,	187,233,38,
	102,150,0,	177,76,
	102,255,255,187,0,
	0,	0,	0,	0,	0,
	0,	0,	0,	0,	0,
	0,	0,	0,	0,	0,

	5, 5, // t
	0,	0,	0,	0,	0,
	0,	0,	0,	0,	0,
	0,	0,	77,	38,	0,
	0,	76,	223,38,	0,
	24,	211,255,255,102,
	0,	38,	212,38,	0,
	0,	38,	212,38,	0,
	0,	38,	212,38,	0,
	0,	38,	212,38,	0,
	0,	24,	211,244,76,
	0,	0,	0,	0,	0,
	0,	0,	0,	0,	0,
	0,	0,	0,	0,	0,

	8, 7, // u
	0,	0,	0,	0,	0,	0,	0,	0,
	0,	0,	0,	0,	0,	0,	0,	0,
	0,	0,	0,	0,	0,	0,	0,	0,
	0,	0,	0,	0,	0,	0,	0,	0,
	38,	233,233,38,	187,244,76,	0,
	0,	38,	212,38,	24,	201,76,	0,
	0,	38,	212,38,	24,	201,76,	0,
	0,	38,	212,38,	24,	201,76,	0,
	0,	38,	212,38,	38,	223,76,	0,
	0,	0,	187,255,244,234,233,38,
	0,	0,	0,	0,	0,	0,	0,	0,
	0,	0,	0,	0,	0,	0,	0,	0,
	0,	0,	0,	0,	0,	0,	0,	0,

	8, 8, // v
	0,	0,	0,	0,	0,	0,	0,	0,
	0,	0,	0,	0,	0,	0,	0,	0,
	0,	0,	0,	0,	0,	0,	0,	0,
	0,	0,	0,	0,	0,	0,	0,	0,
	24,	211,255,187,24,	211,211,24,
	0,	38,	212,38,	0,	90,	76,	0,
	0,	0,	187,150,0,	116,0,	0,
	0,	0,	76,	223,114,90,	0,	0,
	0,	0,	0,	187,211,24,	0,	0,
	0,	0,	0,	102,150,0,	0,	0,
	0,	0,	0,	0,	0,	0,	0,	0,
	0,	0,	0,	0,	0,	0,	0,	0,
	0,	0,	0,	0,	0,	0,	0,	0,

	10, 10, // w
	0,	0,	0,	0,	0,	0,	0,	0,	0,	0,
	0,	0,	0,	0,	0,	0,	0,	0,	0,	0,
	0,	0,	0,	0,	0,	0,	0,	0,	0,	0,
	0,	0,	0,	0,	0,	0,	0,	0,	0,	0,
	38,	233,255,141,233,255,187,76,	244,150,
	0,	76,	201,24,	38,	212,38,	0,	142,24,
	0,	24,	211,102,76,	213,102,38,	128,0,
	0,	0,	102,187,128,114,201,142,38,	0,
	0,	0,	38,	233,150,24,	211,187,0,	0,
	0,	0,	0,	128,38,	0,	90,	76,	0,	0,
	0,	0,	0,	0,	0,	0,	0,	0,	0,	0,
	0,	0,	0,	0,	0,	0,	0,	0,	0,	0,
	0,	0,	0,	0,	0,	0,	0,	0,	0,	0,

	8, 8, // x
	0,	0,	0,	0,	0,	0,	0,	0,
	0,	0,	0,	0,	0,	0,	0,	0,
	0,	0,	0,	0,	0,	0,	0,	0,
	0,	0,	0,	0,	0,	0,	0,	0,
	24,	211,255,187,150,255,102,0,
	0,	24,	211,150,102,102,0,	0,
	0,	0,	38,	233,187,0,	0,	0,
	0,	0,	38,	179,234,76,	0,	0,
	0,	38,	212,38,	102,233,38,	0,
	24,	211,233,38,	187,255,211,24,
	0,	0,	0,	0,	0,	0,	0,	0,
	0,	0,	0,	0,	0,	0,	0,	0,
	0,	0,	0,	0,	0,	0,	0,	0,

	8, 8, // y
	0,	0,	0,	0,	0,	0,	0,	0,
	0,	0,	0,	0,	0,	0,	0,	0,
	0,	0,	0,	0,	0,	0,	0,	0,
	0,	0,	0,	0,	0,	0,	0,	0,
	24,	211,255,187,24,	211,233,38,
	0,	76,	223,38,	0,	102,102,0,
	0,	0,	187,102,0,	142,24,	0,
	0,	0,	76,	223,114,139,0,	0,
	0,	0,	0,	187,244,76,	0,	0,
	0,	0,	0,	76,	177,0,	0,	0,
	0,	0,	0,	102,102,0,	0,	0,
	0,	0,	0,	166,38,	0,	0,	0,
	0,	187,255,102,0,	0,	0,	0,

	7, 7, // z
	0,	0,	0,	0,	0,	0,	0,
	0,	0,	0,	0,	0,	0,	0,
	0,	0,	0,	0,	0,	0,	0,
	0,	0,	0,	0,	0,	0,	0,
	0,	102,255,255,255,244,76,
	0,	102,102,0,	187,102,0,
	0,	0,	0,	150,150,0,	0,
	0,	0,	76,	201,24,	0,	0,
	0,	38,	212,38,	0,	128,38,
	24,	211,255,255,255,233,38,
	0,	0,	0,	0,	0,	0,	0,
	0,	0,	0,	0,	0,	0,	0,
	0,	0,	0,	0,	0,	0,	0,

	5, 3, // {
	0,	0,	0,	0,	0,
	0,	0,	0,	187,187,
	0,	0,	150,187,0,
	0,	0,	187,102,0,
	0,	0,	150,150,0,
	0,	0,	187,102,0,
	24,	211,102,0,	0,
	0,	0,	187,102,0,
	0,	0,	150,150,0,
	0,	0,	187,102,0,
	0,	0,	150,187,0,
	0,	0,	0,	187,187,
	0,	0,	0,	0,	0,

	2, 2, // |
	0,	0,
	38,	128,
	38,	128,
	38,	128,
	38,	128,
	38,	128,
	38,	128,
	38,	128,
	38,	128,
	38,	128,
	38,	128,
	38,	128,
	0,	0,

	5, 3, // }
	0,	0,	0,	0,	0,
	38,	233,150,0,	0,
	0,	24,	211,102,0,
	0,	0,	187,102,0,
	0,	24,	201,76,	0,
	0,	0,	187,150,0,
	0,	0,	0,	150,150,
	0,	0,	150,150,0,
	0,	24,	201,76,	0,
	0,	0,	187,102,0,
	0,	24,	211,102,0,
	38,	233,150,0,	0,
	0,	0,	0,	0,	0,

	8, 8, // ~
	0,	0,	0,	0,	0,	0,	0,	0,
	0,	0,	0,	0,	0,	0,	0,	0,
	0,	0,	0,	0,	0,	0,	0,	0,
	0,	0,	0,	0,	0,	0,	0,	0,
	0,	0,	0,	0,	0,	0,	0,	0,
	0,	0,	0,	0,	0,	0,	0,	0,
	0,	150,255,255,102,0,	102,102,
	24,	167,24,	24,	211,255,211,24,
	0,	0,	0,	0,	0,	0,	0,	0,
	0,	0,	0,	0,	0,	0,	0,	0,
	0,	0,	0,	0,	0,	0,	0,	0,
	0,	0,	0,	0,	0,	0,	0,	0,
	0,	0,	0,	0,	0,	0,	0,	0,

 0};

