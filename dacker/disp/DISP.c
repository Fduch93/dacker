#include "ST7735.h"
#include "ST7735_comm_sm.h"
#include "ST7735_comm.h"

#include "DISP.h"
#include "DISP_hal.h"
#include "DISP_render.h"

#define DEFAULT_INCREMENT_ACTION 0
#define DEFAULT_DECREMENT_ACTION 0

static DISP_Frame_T *currentFrame = 0;
static char backgroundRefresh;

static void (*DISP_incrementAction)(void) = DEFAULT_INCREMENT_ACTION;
static void (*DISP_decrementAction)(void) = DEFAULT_INCREMENT_ACTION;

//#define TRUCK

#ifdef TRUCK
	#include "qwe.log"
#else
	DISP_Color_T backgroundPixelBuilder(uint8_t x, uint8_t y)
	{
		DISP_Color_T color = {0};
		color.r = 192 - y;
		color.g = 192 - y;
		color.b = 192 - y;
		return color;//COLOR_Grey;
	}
#endif

void DISP_Init(DISP_Frame_T *initFrame)
{
	DISP_InitEncoder();

	ST7735_InitStateMachine();
	ST7735_Init();

	DISP_SetFrame(initFrame);
}

DISP_Frame_T *DISP_GetFrame()
{
	return currentFrame;
}

void DISP_SetFrame(DISP_Frame_T *frame)
{
	currentFrame = frame;
	backgroundRefresh = 1;
	DISP_ResetIncDecAction();
}

void DISP_SetIncDecAction(
	void (*incrementAction)(void),
	void (*decrementAction)(void))
{
	DISP_incrementAction = incrementAction;
	DISP_decrementAction = decrementAction;
}

void DISP_ResetIncDecAction()
{
	DISP_incrementAction = DEFAULT_INCREMENT_ACTION;
	DISP_decrementAction = DEFAULT_DECREMENT_ACTION;
}

char DISP_IsIncDecActionSet()
{
	return DISP_incrementAction || DISP_decrementAction;
}

void DISP_Handler()
{
	if (currentFrame->objectCnt == 0)
		return;

	if (backgroundRefresh && ST7735_IsReady())
	{
	#ifdef TRUCK
		ST7735_Render(-1, -2, 162, 132-1, pic);
	#else
		ST7735_FillRect(0,0,160,128, backgroundPixelBuilder);
	#endif
		backgroundRefresh = 0;
		return;
	}

	if (DISP_HAL_nextObject())
	{
		if (DISP_incrementAction)
			DISP_incrementAction();
		else
			DISP_SelectNextObject(currentFrame);
	}
	else if (DISP_HAL_prevObject())
	{
		if (DISP_decrementAction)
			DISP_decrementAction();
		else
			DISP_SelectPrevObject(currentFrame);
	}
	else if (DISP_HAL_selectObject())
	{
//		int selectedIdx = DISP_GetSelectedObjIndex(currentFrame);
		const DISP_Object_T *selectedObject = &currentFrame->objects[currentFrame->selectedObject];
		if (selectedObject->action)
			selectedObject->action();
	}

	if (ST7735_IsReady())
	{
		const DISP_Object_T *object = &currentFrame->objects[currentFrame->currentObject];

		if (object->update)
			object->update(object, currentFrame->currentObject == currentFrame->selectedObject);

		currentFrame->currentObject++;
		currentFrame->currentObject %= currentFrame->objectCnt;
	}
	else
	{
		ST7735_ProcessStateMachine();
	}
}

void DISP_Refresh()
{
	ST7735_ProcessStateMachineUntilDone();

	uint32_t loopObject = currentFrame->currentObject;
	do {
		const DISP_Object_T *object = &currentFrame->objects[currentFrame->currentObject];

		if (object->update)
			object->update(object, currentFrame->currentObject == currentFrame->selectedObject);

		ST7735_ProcessStateMachineUntilDone();

		currentFrame->currentObject++;
		currentFrame->currentObject %= currentFrame->objectCnt;
	} while (currentFrame->currentObject != loopObject);
}
