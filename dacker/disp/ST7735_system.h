#ifndef ST7335_SYSTEM_H
#define ST7335_SYSTEM_H

#include <stdint.h>

void ST7735_SwReset(void);

void ST7735_SleepIn(void);
void ST7735_SleepOut(void);

void ST7735_DisplayOn(void);
void ST7735_DisplayOff(void);

void ST7735_SetPartial(void);
void ST7735_SetNormal(void);

// Frame rate control
typedef struct
{
	union {
		uint8_t u0; 
		struct { uint8_t RTN:4; }; // rate?
	};
	union {
		uint8_t u1; 
		struct { uint8_t FP:6; }; // front porch
	};
	union {
		uint8_t u2; 
		struct { uint8_t BP:6; }; // back porch
	};
} ST7735_FRMCTR_T;

void ST7735_FrameControlFullColor(ST7735_FRMCTR_T *frmctr);
void ST7735_FrameControlIdle(ST7735_FRMCTR_T *frmctr);
void ST7735_FrameControlPartial(ST7735_FRMCTR_T *frmctr1, ST7735_FRMCTR_T *frmctr2);

// Inversion control
#define INVCTR_DOT_INVERSION 0
#define INVCTR_COLUMN_INVERSION 1

typedef struct
{
	union {
		uint8_t u0; 
		struct {
			uint8_t ColInvertFull:1; 
			uint8_t ColInvertIdle:1; 
			uint8_t ColInvertPartial:1; 
			uint8_t pad:5;
		};
	};
} ST7735_INVCTR_T;

void ST7735_InversionControl(ST7735_INVCTR_T *invctr);

void ST7735_InversionOn(void);
void ST7735_InversionOff(void);

// Power control
typedef struct
{
	union {
		uint8_t u0; 
		struct {
			uint8_t VRHP_LO:5; 
			uint8_t AVDD:3;
		};
	};
	union {
		uint8_t u1; 
		struct {
			uint8_t VRHN_LO:5;
			uint8_t pad1:3;
		};
	};
	union {
		uint8_t u2; 
		struct {
			uint8_t VRHP_HI:1;
			uint8_t VRHN_HI:1;
			uint8_t pad2:4;
			uint8_t MODE:3;
		};
	};
} ST7735_PWRCTR1_T;

typedef struct
{
	union {
		uint8_t u0; 
		struct {
			uint8_t VGHBT:2;
			uint8_t VGLSEL:2;
			uint8_t :2;
			uint8_t VGH25:2;
		};
	};
} ST7735_PWRCTR2_T;

typedef struct
{
	union {
		uint8_t u0; 
		struct {
			uint8_t AP:3; 
			uint8_t SAP:3;
			uint8_t DC_HI:2;
		};
	};
	union {
		uint8_t u1; 
		uint8_t DC_LO;
	};
} ST7735_PWRCTR345_T;

typedef struct
{
	union {
		uint8_t u0; 
		struct { uint8_t VCOMS:6; };
	};
} ST7735_VMCTR1_T;

typedef struct
{
	union {
		uint8_t u0; 
		struct { uint8_t VMF:5; };
	};
} ST7735_VMOFCTR_T;

void ST7735_PowerControl1(ST7735_PWRCTR1_T *pwrctr);
void ST7735_PowerControl2(ST7735_PWRCTR2_T *pwrctr);
void ST7735_PowerControl3(ST7735_PWRCTR345_T *pwrctr);
void ST7735_PowerControl4(ST7735_PWRCTR345_T *pwrctr);
void ST7735_PowerControl5(ST7735_PWRCTR345_T *pwrctr);
void ST7735_VComControl(ST7735_VMCTR1_T *vmctr);
void ST7735_VComOffsetControl(ST7735_VMOFCTR_T *vmofctr);

// Color mode (pixel format)
#define COLMOD_12BIT 3
#define COLMOD_16BIT 5
#define COLMOD_18BIT 6

typedef struct
{
	union {
		uint8_t u0; 
		struct { uint8_t IFPF:3; };
	};
} ST7735_COLMOD_T;

void ST7735_SetColorMode(uint8_t mode);

// Gamma control
typedef struct
{
	union {	uint8_t u0;  struct { uint8_t VRF0:5;    }; };
	union {	uint8_t u1;  struct { uint8_t VOS0:5;    }; };
	union {	uint8_t u2;  struct { uint8_t PK0:5;     }; };
	union {	uint8_t u3;  struct { uint8_t PK1:5;     }; };
	union {	uint8_t u4;  struct { uint8_t PK2:5;     }; };
	union {	uint8_t u5;  struct { uint8_t PK3:5;     }; };
	union {	uint8_t u6;  struct { uint8_t PK4:5;     }; };
	union {	uint8_t u7;  struct { uint8_t PK5:5;     }; };
	union {	uint8_t u8;  struct { uint8_t PK6:5;     }; };
	union {	uint8_t u9;  struct { uint8_t PK7:5;     }; };
	union {	uint8_t u10; struct { uint8_t PK8:5;     }; };
	union {	uint8_t u11; struct { uint8_t PK9:5;     }; };
	union {	uint8_t u12; struct { uint8_t SELV0:5;   }; };
	union {	uint8_t u13; struct { uint8_t SELV1:5;   }; };
	union {	uint8_t u14; struct { uint8_t SELV62:5;  }; };
	union {	uint8_t u15; struct { uint8_t SELV63:5;  }; };
} ST7735_GMCTR_T;

void ST7735_GammaControl(ST7735_GMCTR_T *positive, ST7735_GMCTR_T *negative);

// Memory address control
#define MADCTL_LEFT_TO_RIGHT 0
#define MADCTL_RIGHT_TO_LEFT 1

#define MADCTL_RGB 0
#define MADCTL_BGR 1

#define MADCTL_TOP_TO_BOTTOM 0
#define MADCTL_BOTTOM_TO_TOP 1

#define MADCTL_ROW_COL_NORM 0
#define MADCTL_ROW_COL_EXCH 1

#define MADCTL_COL_ORDER_STRAIGHT 0
#define MADCTL_COL_ORDER_REVERSE 1

#define MADCTL_ROW_ORDER_STRAIGHT 0
#define MADCTL_ROW_ORDER_REVERSE 1

typedef struct
{
	union {
		uint8_t u0; 
		struct {
			uint8_t :2;	
			uint8_t MH:1;	// horisontal refresh order
			uint8_t BGR:1;	// RGB/BGR
			uint8_t ML:1;	// vertical refresh order
			uint8_t MV:1;	// row/col exchange
			uint8_t MX:1;	// col order
			uint8_t MY:1;	// row order
		};
	};
} ST7735_MADCTL_T;

void ST7735_MemAddrControl(ST7735_MADCTL_T *madctl);

#endif
