#include "ST7735_system.h"
#include "ST7735_comm.h"
#include "ST7735_commands.h"
#include "SYSTICK.h"

void ST7735_SwReset(void)
{
	ST7735_SimpleCmd(ST7735_SWRESET);
	HAL_Delay(200);
}

void ST7735_SleepIn(void)
{
	ST7735_SimpleCmd(ST7735_SLPIN);
	HAL_Delay(200);
}
void ST7735_SleepOut(void)
{
	ST7735_SimpleCmd(ST7735_SLPOUT);
	HAL_Delay(200);
}

void ST7735_DisplayOn(void)
{
	ST7735_SimpleCmd(ST7735_DISPON);
	HAL_Delay(100);
}

void ST7735_DisplayOff(void)
{
	ST7735_SimpleCmd(ST7735_DISPOFF);
	HAL_Delay(100);
}

void ST7735_SetPartial(void)
{
	ST7735_SimpleCmd(ST7735_PTLON);
	HAL_Delay(10);
}

void ST7735_SetNormal(void)
{
	ST7735_SimpleCmd(ST7735_NORON);
}

void ST7735_FrameControlFullColor(ST7735_FRMCTR_T *frmctr)
{
	ST7735_CmdWithArgs(ST7735_FRMCTR1, (uint8_t *)frmctr, 3);
}

void ST7735_FrameControlIdle(ST7735_FRMCTR_T *frmctr)
{
	ST7735_CmdWithArgs(ST7735_FRMCTR2, (uint8_t *)frmctr, 3);
}

void ST7735_FrameControlPartial(ST7735_FRMCTR_T *frmctr1, ST7735_FRMCTR_T *frmctr2)
{
	ST7735_FRMCTR_T frmctr[2] = {0};
	frmctr[0].RTN = frmctr1->RTN;
	frmctr[0].FP  = frmctr1->FP;
	frmctr[0].BP  = frmctr1->BP;
	frmctr[1].RTN = frmctr2->RTN;
	frmctr[1].FP  = frmctr2->FP;
	frmctr[1].BP  = frmctr2->BP;
	ST7735_CmdWithArgs(ST7735_FRMCTR3, (uint8_t *)frmctr, 6);
}

void ST7735_InversionControl(ST7735_INVCTR_T *invctr)
{
	invctr->pad = 0b00000;
	ST7735_CmdWithArgs(ST7735_INVCTR, (uint8_t *)invctr, 1);
}

void ST7735_InversionOn(void)
{
	ST7735_SimpleCmd(ST7735_INVON);
}

void ST7735_InversionOff(void)
{
	ST7735_SimpleCmd(ST7735_INVOFF);
}

void ST7735_PowerControl1(ST7735_PWRCTR1_T *pwrctr)
{
	pwrctr->pad1 = 0b000;
	pwrctr->pad2 = 0b0001;
	ST7735_CmdWithArgs(ST7735_PWCTR1, (uint8_t *)pwrctr, 3);
}

void ST7735_PowerControl2(ST7735_PWRCTR2_T *pwrctr)
{
	ST7735_CmdWithArgs(ST7735_PWCTR2, (uint8_t *)pwrctr, 1);
}

void ST7735_PowerControl3(ST7735_PWRCTR345_T *pwrctr)
{
	ST7735_CmdWithArgs(ST7735_PWCTR3, (uint8_t *)pwrctr, 2);
}

void ST7735_PowerControl4(ST7735_PWRCTR345_T *pwrctr)
{
	ST7735_CmdWithArgs(ST7735_PWCTR4, (uint8_t *)pwrctr, 2);
}

void ST7735_PowerControl5(ST7735_PWRCTR345_T *pwrctr)
{
	ST7735_CmdWithArgs(ST7735_PWCTR5, (uint8_t *)pwrctr, 2);
}

void ST7735_VComControl(ST7735_VMCTR1_T *vmctr)
{
	ST7735_CmdWithArgs(ST7735_VMCTR1, (uint8_t *)vmctr, 1);
}

void ST7735_VComOffsetControl(ST7735_VMOFCTR_T *vmofctr)
{
	ST7735_CmdWithArgs(ST7735_VMOFCTR, (uint8_t *)vmofctr, 1);
}

void ST7735_SetColorMode(uint8_t mode)
{
	ST7735_COLMOD_T colmod;
	colmod.IFPF = mode;
	ST7735_CmdWithArgs(ST7735_COLMOD, (uint8_t *)&colmod, 1);
}

void ST7735_GammaControl(ST7735_GMCTR_T *positive, ST7735_GMCTR_T *negative)
{
	ST7735_CmdWithArgs(ST7735_GMCTRP1, (uint8_t *)positive, 16);
	ST7735_CmdWithArgs(ST7735_GMCTRN1, (uint8_t *)negative, 16);
}

void ST7735_MemAddrControl(ST7735_MADCTL_T *madctl)
{
	ST7735_CmdWithArgs(ST7735_MADCTL, (uint8_t *)madctl, 1);	
}
