#ifndef DISP_RENDER_H
#define DISP_RENDER_H

#include "DISP_object.h"
#include "DISP_color.h"

uint8_t *GetObjectBuffer(void);

void ST7735_Render(uint8_t left, uint8_t top, uint8_t right, uint8_t bottom, const uint8_t *buf);
void ST7735_DrawObject(const DISP_Object_T *obj);
void ST7735_FillRect(
	uint8_t left,
	uint8_t top,
	uint8_t right,
	uint8_t bottom,
	DISP_Color_T (*pixelBuilder)(uint8_t x, uint8_t y));

#endif
