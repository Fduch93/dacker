#ifndef DISP_MULTILINE_H
#define DISP_MULTILINE_H

#include "DISP_text.h"

typedef struct
{
	const char *pattern;
	uint32_t var;
} DISP_Line_T;

typedef enum
{
	DISP_MULTILINE_STRAIGHT,
	DISP_MULTILINE_REVERSED
} DISP_MultilineDirection_T;

void DISP_DrawMultiline(
	const DISP_Object_T *obj,
	const Font_T *font,
	const DISP_Line_T *text,
	const uint32_t lineCnt,
	DISP_Color_T fgColor,
	DISP_Color_T bgColor,
	uint32_t startLine,
	uint32_t *pCurrentLine,
	DISP_MultilineDirection_T direction);

#endif
