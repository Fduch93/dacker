#include "DISP_text.h"
#include "DISP_render.h"
#include <string.h>

void DISP_DrawText(
	const DISP_Object_T *obj,
	const Font_T *font,
	const char *text,
	DISP_Color_T fgColor,
	DISP_Color_T bgColor,
	const TextAlignHorz_T horz,
	const TextAlignVert_T vert,
	const char isSelected)
{
	DISP_Rect_T objRect = {
		obj->bounds.left,
		obj->bounds.top,
		obj->bounds.right - obj->bounds.left,
		obj->bounds.bottom - obj->bounds.top};

	DISP_Rect_T textRect;

	uint8_t objWidth = objRect.width;
	uint8_t textWidth = font->getStringWidth(text);

	if (textWidth < objWidth)
	{
		switch (horz)
		{
			case TEXT_HLEFT:
				textRect.left = 0;
				break;
			case TEXT_HCENTER:
				textRect.left = (objWidth - textWidth) >> 1;
				break;
			case TEXT_HRIGHT:
				textRect.left = objWidth - textWidth;
				break;
			default:
				return;
		}
		textRect.width = textWidth;
	}
	else
	{
		textRect.left = obj->bounds.left;
		textRect.width = objWidth;
	}

	uint8_t objHeight = objRect.height;
	uint8_t textHeight = font->getHeight();

	if (textWidth < objWidth)
	{
		switch (vert)
		{
			case TEXT_VTOP:
				textRect.top = 0;
				break;
			case TEXT_VCENTER:
				textRect.top = (objHeight - textHeight) >> 1;
				break;
			case TEXT_VBOTTOM:
				textRect.top = objHeight - textHeight;
				break;
			default:
				return;
		}
		textRect.height = textHeight;
	}
	else
	{
		textRect.top = 0;
		textRect.height = objHeight;
	}

	uint8_t *buf = GetObjectBuffer();

	char objEnabled;
	if (obj->isEnabled == ALWAYS_ENABLED)
		objEnabled = 1;
	else
		objEnabled = obj->isEnabled();
	fgColor = objEnabled ? fgColor : COLOR_Grey;
	bgColor = objEnabled ? bgColor : COLOR_DarkGrey;

	for (int i = 0; i < objRect.width * objRect.height; i++)
	{
		buf[i*3+0] = bgColor.r;
		buf[i*3+1] = bgColor.g;
		buf[i*3+2] = bgColor.b;
	}

	font->writeString(
		text,
		buf,
		objRect,
		textRect,
		fgColor,
		bgColor);

	if (isSelected)
	{
		int lastLineShift = objRect.width * (objRect.height - 1) * 3;

		for (int ix = 0; ix < objRect.width*3; ix += 3)
		{
			buf[ix+0] = fgColor.r;
			buf[ix+1] = fgColor.g;
			buf[ix+2] = fgColor.b;
			buf[ix+lastLineShift+0] = fgColor.r;
			buf[ix+lastLineShift+1] = fgColor.g;
			buf[ix+lastLineShift+2] = fgColor.b;
		}

		int lastColShift = (objRect.width - 1) * 3;

		for (int y = 1; y < objRect.height - 1; y++)
		{
			buf[y*objRect.width*3+0] = fgColor.r;
			buf[y*objRect.width*3+1] = fgColor.g;
			buf[y*objRect.width*3+2] = fgColor.b;
			buf[y*objRect.width*3+lastColShift+0] = fgColor.r;
			buf[y*objRect.width*3+lastColShift+1] = fgColor.g;
			buf[y*objRect.width*3+lastColShift+2] = fgColor.b;
		}
	}

	ST7735_DrawObject(obj);
}

#define LINE_BUF_SIZE 256
static char lineBuf[LINE_BUF_SIZE];

char *DISP_MakeString(const char *pattern, uint32_t var)
{
	if (!pattern)
		return "\0";

	const char *wildchar = strchr(pattern, '%');
	if (wildchar == 0)
	{
		uint32_t len = strlen(pattern);
		len = (len < (LINE_BUF_SIZE-1)) ? (len) : (LINE_BUF_SIZE-1);
		memcpy(lineBuf, pattern, len);
		lineBuf[len] = '\0';
		return lineBuf;
	}
	else
	{
		switch (*(wildchar+1))
		{
			case 's':
			{
				memcpy(lineBuf, pattern, wildchar - pattern);

				char *str = (char *)var;
				uint32_t strSz = strlen(str);
				memcpy(lineBuf + (wildchar - pattern), str, strSz);

				uint32_t len = strlen(pattern);
				memcpy(lineBuf + (wildchar - pattern) + strSz, wildchar+2, len - (wildchar+2 - pattern));
				lineBuf[len-2 + strSz] = '\0';
				return lineBuf;
			}
			case 'x':
			case 'd':
			case 'l':
			{
				memcpy(lineBuf, pattern, wildchar - pattern);

				char str[11] = "0000000000\0";

				uint32_t cntBase = *(wildchar+1) == 'x' ? 16 : 10;

				char *c = &str[10];
				for (uint32_t div = 1; c > str; div *= cntBase)
				{
					uint32_t quantient = (var / div);
					if (quantient > 0 || div == 1)
					{
						c--;
						uint32_t digit = quantient % cntBase;
						if (digit < 10)
							*c = '0' + quantient % cntBase;
						else
							*c = 'A' + quantient % cntBase - 10;
					}
					else break;
				}

				uint32_t strSz = strlen(c);
				memcpy(lineBuf + (wildchar - pattern), c, strSz);

				uint32_t len = strlen(pattern);
				memcpy(lineBuf + (wildchar - pattern) + strSz, wildchar+2, len - (wildchar+2 - pattern));
				lineBuf[len-2 + strSz] = '\0';
				return lineBuf;
			}

			default:
			{
				uint32_t len = strlen(pattern);
				len = (len < (LINE_BUF_SIZE-1)) ? (len) : (LINE_BUF_SIZE-1);
				memcpy(lineBuf, pattern, len);
				lineBuf[len] = '\0';
				return lineBuf;
			}
		}
	}
	// return "make string error"; // unreachable
}
