#include "ST7735_hal.h"
#include "ST7735_system.h"

void ST7735_Init(void)
{
	ST7735_HAL_Init();

	ST7735_HAL_ChipDeselect();
	ST7735_HAL_OnByReset();
	ST7735_HAL_OffByReset();
	ST7735_HAL_OnByReset();

	ST7735_SwReset();
	ST7735_SleepOut();

	ST7735_FRMCTR_T frmctr;
	frmctr.RTN = 0x01;
	frmctr.FP = 0x2C;
	frmctr.BP = 0x2D;
	ST7735_FrameControlFullColor(&frmctr);
	ST7735_FrameControlIdle(&frmctr);
	ST7735_FrameControlPartial(&frmctr,&frmctr);

	ST7735_INVCTR_T invctr;
	invctr.ColInvertFull = INVCTR_COLUMN_INVERSION;
	invctr.ColInvertIdle = INVCTR_COLUMN_INVERSION;
	invctr.ColInvertPartial = INVCTR_COLUMN_INVERSION;
	ST7735_InversionControl(&invctr);

	ST7735_PWRCTR1_T pwrctr1;
	pwrctr1.VRHP_LO = 2;
	pwrctr1.AVDD = 5;
	pwrctr1.VRHN_LO = 2;
	pwrctr1.MODE = 2;
	pwrctr1.VRHP_HI = 0;
	pwrctr1.VRHN_HI = 0;
	ST7735_PowerControl1(&pwrctr1);

	ST7735_PWRCTR2_T pwrctr2;
	pwrctr2.VGHBT = 1;
	pwrctr2.VGLSEL = 1;
	pwrctr2.VGH25 = 3;
	ST7735_PowerControl2(&pwrctr2);

	ST7735_PWRCTR345_T pwrctr345;
	pwrctr345.AP = 2;
	pwrctr345.SAP = 1;
	pwrctr345.DC_HI = 0;
	pwrctr345.DC_LO = 0;
	ST7735_PowerControl3(&pwrctr345);

	pwrctr345.AP = 2;
	pwrctr345.SAP = 1;
	pwrctr345.DC_HI = 2;
	pwrctr345.DC_LO = 0x28;
	ST7735_PowerControl4(&pwrctr345);

	pwrctr345.AP = 2;
	pwrctr345.SAP = 1;
	pwrctr345.DC_HI = 2;
	pwrctr345.DC_LO = 0xEE;
	ST7735_PowerControl5(&pwrctr345);

	ST7735_VMCTR1_T vmctr;
	vmctr.VCOMS = 0xE;
	ST7735_VComControl(&vmctr);

	ST7735_InversionOff();

	ST7735_SetColorMode(COLMOD_18BIT);

	ST7735_GMCTR_T gmctrp1 =
	{
		{0x02}, {0x1c}, {0x07}, {0x12},
		{0x37}, {0x32}, {0x29}, {0x2d},
		{0x29}, {0x25}, {0x2B}, {0x39},
		{0x00}, {0x01}, {0x03}, {0x10}
	};
	ST7735_GMCTR_T gmctrn1 =
	{
		{0x03}, {0x1d}, {0x07}, {0x06},
		{0x2E}, {0x2C}, {0x29}, {0x2d},
		{0x2E}, {0x2E}, {0x37}, {0x3F},
		{0x00}, {0x00}, {0x02}, {0x10}
	};
	ST7735_GammaControl(&gmctrp1, &gmctrn1);

	ST7735_SetNormal();
	ST7735_DisplayOn();

	ST7735_MADCTL_T madctl;
	madctl.u0 = 0;
	madctl.MX = 0;
	madctl.MY = 1;
	madctl.MV = MADCTL_ROW_COL_EXCH;
	ST7735_MemAddrControl(&madctl);
}
