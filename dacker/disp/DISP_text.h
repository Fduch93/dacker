#ifndef DISP_TEXT_H
#define DISP_TEXT_H

#include "DISP_object.h"
#include "DISP_color.h"
#include "DISP_font.h"

typedef enum
{
	TEXT_HLEFT,
	TEXT_HCENTER,
	TEXT_HRIGHT
} TextAlignHorz_T;

typedef enum
{
	TEXT_VTOP,
	TEXT_VCENTER,
	TEXT_VBOTTOM
} TextAlignVert_T;

typedef struct
{
	const char *text;
	const Font_T *font;

	DISP_Color_T fgColor;
	DISP_Color_T bgColor;

	TextAlignHorz_T horz;
	TextAlignVert_T vert;
} TextData_T;

void DISP_DrawText(
	const DISP_Object_T *obj,
	const Font_T *font,
	const char *text,
	DISP_Color_T fgColor,
	DISP_Color_T bgColor,
	const TextAlignHorz_T horz,
	const TextAlignVert_T vert,
	const char isSelected);

char *DISP_MakeString(const char *pattern, uint32_t var);

#endif
