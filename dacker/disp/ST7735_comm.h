#ifndef ST7335_COMM_H
#define ST7335_COMM_H

#include <stdint.h>

void ST7735_SimpleCmd(uint8_t cmd);
void ST7735_CmdWithArgs(uint8_t cmd, const uint8_t *args, uint16_t args_len);
void ST7735_SimpleRequest(uint8_t cmd, uint8_t *response, uint16_t resp_len);
void ST7735_SimpleRequestNoDummyRead(uint8_t cmd, uint8_t *response, uint16_t resp_len);

void ST7735_StartCmdWithArgs(uint8_t cmd, const uint8_t *args, uint16_t args_len);

#endif
