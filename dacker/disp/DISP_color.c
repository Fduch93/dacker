#include "DISP_color.h"

const DISP_Color_T COLOR_White			= {0x00FFFFFF};
const DISP_Color_T COLOR_Silver			= {0x00C0C0C0};
const DISP_Color_T COLOR_Grey			= {0x00808080};
const DISP_Color_T COLOR_DarkGrey		= {0x00404040};
const DISP_Color_T COLOR_Black			= {0x00000000};

const DISP_Color_T COLOR_Red			= {0x00FF0000};
const DISP_Color_T COLOR_MediumRed		= {0x00C00000};
const DISP_Color_T COLOR_Maroon			= {0x00800000};
const DISP_Color_T COLOR_DarkRed		= {0x00400000};

const DISP_Color_T COLOR_Blue			= {0x000000FF};
const DISP_Color_T COLOR_MediumBlue		= {0x000000C0};
const DISP_Color_T COLOR_Navy			= {0x00000080};
const DISP_Color_T COLOR_DarkBlue		= {0x00000040};

const DISP_Color_T COLOR_Lime			= {0x0000FF00};
const DISP_Color_T COLOR_MediumGreen	= {0x0000C000};
const DISP_Color_T COLOR_Green			= {0x00008000};
const DISP_Color_T COLOR_DarkGreen		= {0x00004000};

const DISP_Color_T COLOR_Yellow			= {0x00FFFF00};
const DISP_Color_T COLOR_Cyan			= {0x0000FFFF};
const DISP_Color_T COLOR_Magenta		= {0x00FF00FF};

const DISP_Color_T COLOR_DarkYellow		= {0x00404000};
