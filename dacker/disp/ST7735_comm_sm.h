#ifndef ST7335_COMM_SM_H
#define ST7335_COMM_SM_H

#include <stdint.h>

void ST7735_InitStateMachine(void);
char ST7735_IsReady(void);
void ST7735_ProcessStateMachine(void);
void ST7735_ProcessStateMachineUntilDone(void);
void ST7735_StartRequest(uint8_t cmd, const uint8_t *request, uint16_t req_len, uint8_t *response, uint16_t resp_len, char read_delay);

#endif
