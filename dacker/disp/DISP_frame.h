#ifndef DISP_FRAME_H
#define DISP_FRAME_H

#include "DISP_object.h"

typedef struct
{
	const DISP_Object_T *objects;
	uint32_t objectCnt;
	uint32_t currentObject;
	uint32_t selectedObject;
} DISP_Frame_T;

void DISP_SelectNextObject(DISP_Frame_T *frame);
void DISP_SelectPrevObject(DISP_Frame_T *frame);

#endif
