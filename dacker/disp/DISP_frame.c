#include "DISP_frame.h"

static char DISP_IsObjectEnabled(const DISP_Object_T *obj)
{
	if (obj->isEnabled == ALWAYS_ENABLED)
		return 1;
	else if (obj->isEnabled())
		return 1;
	else
		return 0;
}

void DISP_SelectNextObject(DISP_Frame_T *frame)
{
	const DISP_Object_T *objects = frame->objects;
	uint32_t objectCnt = frame->objectCnt;

	int oldSelected = frame->selectedObject;

	int newSelected = (oldSelected + 1) % objectCnt;
	while (newSelected != oldSelected)
	{
		if ((objects[newSelected].action && DISP_IsObjectEnabled(&objects[newSelected])) || newSelected == oldSelected)
			break;
		newSelected = (newSelected + 1) % objectCnt;
	}
	frame->selectedObject = newSelected;
}

void DISP_SelectPrevObject(DISP_Frame_T *frame)
{
	const DISP_Object_T *objects = frame->objects;
	uint32_t objectCnt = frame->objectCnt;

	int oldSelected = frame->selectedObject;

	int newSelected =
		(oldSelected == 0)
			? (objectCnt - 1)
			: (oldSelected - 1) % objectCnt;
	while (newSelected != oldSelected)
	{
		if ((objects[newSelected].action && DISP_IsObjectEnabled(&objects[newSelected])) || newSelected == oldSelected)
			break;
		newSelected =
			(newSelected == 0)
				? (objectCnt - 1)
				: (newSelected - 1) % objectCnt;
	}
	frame->selectedObject = newSelected;
}
