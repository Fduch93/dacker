#ifndef UI_SCOPE_H
#define UI_SCOPE_H

#include "DISP_object.h"

void UI_ChSelButtonUpdate(const DISP_Object_T *obj, char isSelected);
void UI_ChSelButtonAction(void);

void UI_ScopeUpdate(const DISP_Object_T *obj, char isSelected);
void UI_ScopeAction(void);

void UI_ZoomButtonUpdate(const DISP_Object_T *obj, char isSelected);
void UI_ZoomButtonAction(void);

void UI_SkipButtonUpdate(const DISP_Object_T *obj, char isSelected);
void UI_SkipButtonAction(void);

#endif
