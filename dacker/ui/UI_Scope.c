#include "DISP_color.h"
#include "DISP_text.h"
#include "DISP_render.h"
#include "DISP.h"
#include "tnr12.h"
#include "SYSTICK.h"
#include <string.h>
#include "wave.h"

static union {
	uint16_t u16;
	struct {
		uint8_t low;
		uint8_t high;
	};
} scopeData[160] = {0};

extern Wave_T wave1;
static uint8_t selMode = 0;

void UI_ScopeUpdate(const DISP_Object_T *obj, char isSelected)
{
	uint32_t scopeWidth = obj->bounds.right - obj->bounds.left;
	uint32_t height = obj->bounds.bottom - obj->bounds.top;

	uint8_t *buf = GetObjectBuffer();

	DISP_Color_T bgColor = COLOR_Black;
	if (isSelected)	bgColor = COLOR_Grey;
	else bgColor = COLOR_DarkGrey;
	
	for (uint8_t x = 0; x < scopeWidth; x++)
	{
		scopeData[x].low = 0xFF;
		scopeData[x].high = 0x00;
	}
		
	for (uint32_t s = 0; s < WAVE_SZ; s++)
	{
		uint16_t smpl = wave1.samples[s];
		uint8_t val = smpl*height/(1U<<12);
		uint8_t x = (s+1)*scopeWidth/WAVE_SZ;
		if (val > scopeData[x].high)
			scopeData[x].high = val;
		if (val < scopeData[x].low)
			scopeData[x].low = val;
	}

	uint32_t strobeStartX = WAVE_GetStrobeStart()*scopeWidth/WAVE_SZ;
	uint32_t strobeEndX = strobeStartX + WAVE_GetStrobeWidth()*scopeWidth/WAVE_SZ;
	for (uint8_t x = 0; x < scopeWidth; x++)
	{
		for (uint8_t y = 0; y < height; y++)
		{
			DISP_Color_T color;
			if (scopeData[x].low <= y && y <= scopeData[x].high)
				color = COLOR_Lime;
			else if (y == height/2)
				color = COLOR_DarkGreen;
			else if (x > strobeStartX && x < strobeEndX)
			{
				if (selMode == 1)
					color = COLOR_Yellow;
				else if (selMode == 2)
					color = COLOR_Cyan;
				else 
					color = COLOR_Green;
			}
			else
				color = bgColor;
			uint32_t pixelAddress = (x + (height - (y+1)) * scopeWidth) * 3;
			buf[pixelAddress + 0] = color.r;
			buf[pixelAddress + 1] = color.g;
			buf[pixelAddress + 2] = color.b;
		}
	}

	ST7735_DrawObject(obj);
}

static void UI_MoveStrobeRight(void);
static void UI_MoveStrobeLeft(void);
static void UI_StrobeWide(void);
static void UI_StrobeNarrow(void);

void UI_ScopeAction()
{
	switch (selMode)
	{
		case 0:
			selMode = 1;
			DISP_SetIncDecAction(UI_MoveStrobeRight, UI_MoveStrobeLeft);
			break;
		case 1:
			selMode = 2;
			DISP_SetIncDecAction(UI_StrobeWide, UI_StrobeNarrow);
			break;
		default:
			DISP_ResetIncDecAction();
			selMode = 0;
			break;
	}
}

static void UI_MoveStrobeRight()
{
	uint32_t strobeStart = WAVE_GetStrobeStart();
	uint32_t strobeWidth = WAVE_GetStrobeWidth();
	if (strobeStart + strobeWidth < WAVE_SZ - WAVE_SZ/32)
		WAVE_SetStrobeStart(strobeStart + WAVE_SZ/32);
}

static void UI_MoveStrobeLeft()
{
	uint32_t strobeStart = WAVE_GetStrobeStart();
	uint32_t strobeWidth = WAVE_GetStrobeWidth();
	if (strobeStart > WAVE_SZ/32)
		WAVE_SetStrobeStart(strobeStart - WAVE_SZ/32);
}


static void UI_StrobeWide()
{
	uint32_t strobeStart = WAVE_GetStrobeStart();
	uint32_t strobeWidth = WAVE_GetStrobeWidth();
	if (strobeStart + strobeWidth < WAVE_SZ - WAVE_SZ/64)
		WAVE_SetStrobeWidth(strobeWidth + WAVE_SZ/64);
}

static void UI_StrobeNarrow()
{
	uint32_t strobeWidth = WAVE_GetStrobeWidth();
	if (strobeWidth > WAVE_SZ/64)
		WAVE_SetStrobeWidth(strobeWidth - WAVE_SZ/64);
}
