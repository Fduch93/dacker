#include "DISP_frame.h"
#include "UI_Scope.h"
#include "DISP.h"
#include "DISP_text.h"
#include "tnr12.h"
#include "wave.h"
#include "num2str.h"
#include <string.h>

static void UI_ModeButtonUpdate(const DISP_Object_T *obj, char isSelected);
static void UI_ModeButtonAction(void);

static void UI_FreqButtonUpdate(const DISP_Object_T *obj, char isSelected);
static void UI_FreqButtonAction(void);
static void UI_IncrementFreq(void);
static void UI_DecrementFreq(void);

static void UI_AmplButtonUpdate(const DISP_Object_T *obj, char isSelected);
static void UI_AmplButtonAction(void);
static void UI_IncrementAmpl(void);
static void UI_DecrementAmpl(void);

static void UI_DistButtonUpdate(const DISP_Object_T *obj, char isSelected);
static void UI_DistButtonAction(void);
static void UI_IncrementDist(void);
static void UI_DecrementDist(void);

void UI_OnOffButtonUpdate(const DISP_Object_T *obj, char isSelected);
void UI_OnOffButtonAction(void);

void UI_SrobeStartUpdate(const DISP_Object_T *obj, char isSelected);
void UI_StrobeWidthUpdate(const DISP_Object_T *obj, char isSelected);

static const DISP_Object_T scopeFrameObjects[] =
{
	{{  0, 93, 32,110},	UI_OnOffButtonUpdate,	UI_OnOffButtonAction,	ALWAYS_ENABLED},
	{{ 33, 93, 96,110},	UI_SrobeStartUpdate,	OBJECT_NO_ACTION,		ALWAYS_ENABLED},
	{{ 97, 93,160,110},	UI_StrobeWidthUpdate,	OBJECT_NO_ACTION,		ALWAYS_ENABLED},
	
	{{  0,111, 32,128},	UI_ModeButtonUpdate,	UI_ModeButtonAction,	ALWAYS_ENABLED},
	{{ 33,111, 88,128},	UI_FreqButtonUpdate,	UI_FreqButtonAction,	ALWAYS_ENABLED},
	{{ 89,111,126,128},	UI_AmplButtonUpdate,	UI_AmplButtonAction,	ALWAYS_ENABLED},
	{{127,111,160,128},	UI_DistButtonUpdate,	UI_DistButtonAction,	ALWAYS_ENABLED},
	
	{{  0,  0,160, 92},	UI_ScopeUpdate,			UI_ScopeAction,			ALWAYS_ENABLED},
};

#define scopeFrameObjectCount (sizeof(scopeFrameObjects) / sizeof(DISP_Object_T))

static DISP_Frame_T scopeFrame = {
	scopeFrameObjects,
	scopeFrameObjectCount,
	0
};

DISP_Frame_T *UI_GetScopeFrame()
{
	return &scopeFrame;
}

void UI_SrobeStartUpdate(const DISP_Object_T *obj, char isSelected)
{
	char text[32] = "S:\0";
	
	num2dec(text, WAVE_GetStrobeStart());
	strcat(text, "(");
	num2dec(text, WAVE_SZ);
	strcat(text, ")");

	DISP_DrawText(
		obj,
		&TNR12,
		text,
		COLOR_DarkYellow,
		COLOR_Yellow,
		TEXT_HCENTER,
		TEXT_VCENTER,
		isSelected);
}

void UI_StrobeWidthUpdate(const DISP_Object_T *obj, char isSelected)
{
	char text[32] = "W:\0";
	
	num2dec(text, WAVE_GetStrobeWidth());
	strcat(text, "(");
	num2dec(text, WAVE_SZ);
	strcat(text, ")");

	DISP_DrawText(
		obj,
		&TNR12,
		text,
		COLOR_DarkBlue,
		COLOR_Cyan,
		TEXT_HCENTER,
		TEXT_VCENTER,
		isSelected);
}

void UI_ModeButtonUpdate(const DISP_Object_T *obj, char isSelected)
{
	char *text = 
		WAVE_GetMode() == DacMode_Sin
			? "SIN"
			: "SQR";

	DISP_DrawText(
		obj,
		&TNR12,
		text,
		(DISP_IsIncDecActionSet() && isSelected) ? COLOR_DarkGreen : COLOR_MediumGreen,
		(DISP_IsIncDecActionSet() && isSelected) ? COLOR_MediumGreen : COLOR_DarkGreen,
		TEXT_HCENTER,
		TEXT_VCENTER,
		isSelected);
}

void UI_ModeButtonAction()
{
	WAVE_SetMode(
		WAVE_GetMode() == DacMode_Sin
			? DacMode_Sqr
			: DacMode_Sin);
}

void UI_FreqButtonUpdate(const DISP_Object_T *obj, char isSelected)
{
	char text[32] = "\0";
	
	num2dec(text, WAVE_GetFreq());
	strcat(text, "Hz");

	DISP_DrawText(
		obj,
		&TNR12,
		text,
		(DISP_IsIncDecActionSet() && isSelected) ? COLOR_DarkGreen : COLOR_MediumGreen,
		(DISP_IsIncDecActionSet() && isSelected) ? COLOR_MediumGreen : COLOR_DarkGreen,
		TEXT_HCENTER,
		TEXT_VCENTER,
		isSelected);
}

void UI_FreqButtonAction()
{
	if (DISP_IsIncDecActionSet())
		DISP_ResetIncDecAction();
	else
		DISP_SetIncDecAction(UI_IncrementFreq, UI_DecrementFreq);
}

static void UI_IncrementFreq()
{
	uint32_t freq = WAVE_GetFreq();
	if (freq < 10000)
		WAVE_SetFreq(freq + (4*(freq/5000)+1)*100);
}

static void UI_DecrementFreq()
{
	uint32_t freq = WAVE_GetFreq();
	if (freq > 100)
		WAVE_SetFreq(freq - (4*(freq/5001)+1)*100);
}

void UI_AmplButtonUpdate(const DISP_Object_T *obj, char isSelected)
{
	char text[32] = "\0";;
	text[0] = '\0';
	
	num2dec(text, WAVE_GetAmpl());

	DISP_DrawText(
		obj,
		&TNR12,
		text,
		(DISP_IsIncDecActionSet() && isSelected) ? COLOR_DarkGreen : COLOR_MediumGreen,
		(DISP_IsIncDecActionSet() && isSelected) ? COLOR_MediumGreen : COLOR_DarkGreen,
		TEXT_HCENTER,
		TEXT_VCENTER,
		isSelected);
}

void UI_AmplButtonAction()
{
	if (DISP_IsIncDecActionSet())
		DISP_ResetIncDecAction();
	else
		DISP_SetIncDecAction(UI_IncrementAmpl, UI_DecrementAmpl);
}

static void UI_IncrementAmpl()
{
	uint32_t ampl = WAVE_GetAmpl();
	if (ampl < 2048)
		WAVE_SetAmpl(ampl + 1);
}

static void UI_DecrementAmpl()
{
	uint32_t ampl = WAVE_GetAmpl();
	if (ampl > 64)
		WAVE_SetAmpl(ampl - 1);
}

void UI_DistButtonUpdate(const DISP_Object_T *obj, char isSelected)
{
	char text[32] = "D\0";
	
	num2dec(text, WAVE_GetDist());

	DISP_DrawText(
		obj,
		&TNR12,
		text,
		(DISP_IsIncDecActionSet() && isSelected) ? COLOR_DarkGreen : COLOR_MediumGreen,
		(DISP_IsIncDecActionSet() && isSelected) ? COLOR_MediumGreen : COLOR_DarkGreen,
		TEXT_HCENTER,
		TEXT_VCENTER,
		isSelected);
}

void UI_DistButtonAction()
{
	if (DISP_IsIncDecActionSet())
		DISP_ResetIncDecAction();
	else
		DISP_SetIncDecAction(UI_IncrementDist, UI_DecrementDist);
}

static void UI_IncrementDist()
{
	uint32_t dist = WAVE_GetDist();
	if (dist < 100)
		WAVE_SetDist(dist + 2);
}

static void UI_DecrementDist()
{
	uint32_t dist = WAVE_GetDist();
	if (dist > 0)
		WAVE_SetDist(dist - 2);
}

void UI_OnOffButtonUpdate(const DISP_Object_T *obj, char isSelected)
{
	char recRunning = WAVE_IsRunning();

	DISP_DrawText(
		obj,
		&TNR12,
		recRunning ? "ON" : "OFF",
		COLOR_DarkRed,
		recRunning ? COLOR_Red : COLOR_Silver,
		TEXT_HCENTER,
		TEXT_VCENTER,
		isSelected);
}

void UI_OnOffButtonAction(void)
{
	if (!WAVE_IsRunning())
		WAVE_Start();
	else
		WAVE_Stop();
}
